'use strict'
function print(text){//{{{
  console.log(text);
}//}}}
var construct=function(parent,child){//{{{
  child.prototype=Object.create(parent.prototype);
  child.prototype.constructor=child;
  return child.prototype;
}//}}}

function mbWidget(mb,master,className){//{{{
  this.mb=mb;
  this.master=this.__getMaster(master);
  this.widget=this.__prepare(className);
}
var def=mbWidget.prototype;
def.__prepare=function(className){//{{{
  var w=document.createElement("div");
  w.className=className;
  this.master.appendChild(w);
  return w;
}//}}}
def.__getMaster=function(master){//{{{
  if(master===null){
    return document.body;
  }else{
    return master;
  }
}//}}}
//}}}
function mbCommunication(mb,socket){//{{{
  this.mb=mb;
  this.socket=socket;
}
var def=mbCommunication.prototype;
def.sendFormData=function(command,args,formName,cb){//{{{
  var d={
    command:{
      func:command,
      args:args,
      formName:formName
    }
  },
  result=JSON.stringify(d);
  this.__socket.emit("formData",result,cb);
  return JSON.stringify(d,undefined,2)
}//}}}
//}}}
function mbFormCommunication(mb,socket){//{{{
  mbCommunication.call(this,mb,socket);
}
var def=construct(mbCommunication,mbFormCommunication);
def.startForm=function(formName){//{{{
  this.sendData("form.start.byName",formName,function(res){
    this.mb.formW.setData(res);
    this.mb.formW.show();
  }.bind(this))
}//}}}
def.sendFormItem=function(data){//{{{
  this.sendData("form.go.next",data,function(res){
    if(res.continue){
      this.mb.formW.setFormItem(res.item);
      this.mb.formW.setProgress(res.progress);
    }else{
      this.mb.formW.hide();
    }
  }.bind(this))
}//}}}
def.sendForPreviousFormItem=function(data){//{{{
  this.sendData("form.go.previous",data,function(res){
    this.mb.formW.setFormItem(res.item);
    this.mb.formW.setProgress(res.progress);
  }.bind(this))
}//}}}
def.sendData=function(command,args,cb){//{{{
  var d={
    command:{
      func:command,
      args:args
    }
  },
  result=JSON.stringify(d);
  this.socket.emit("formData",result,cb);
  return JSON.stringify(d,undefined,2);
}//}}}
def.receiveData=function(cb){//{{{
  
}//}}}
//}}}
function mbForm(mb,master){//{{{
  mbWidget.call(this,mb,master,"mbForm");
  this.content=null;
  this.prepare();
  this.__currentForm=null;
  this.__currentFormItem=null;
  this.__currentLabel=null;
  this.__currentID=null;
}
var def=construct(mbWidget,mbForm);
def.prepare=function(){//{{{
  this.content=document.createElement("div");
  this.content.className="mbFormContent";
  this.__createTitle();
  this.widget.appendChild(this.content);
  this.__createMiddleContent();
  this.__createCommentBar();
  this.__createProgressBar();
  this.__createInfoWidget();
}//}}}
def.__createTitle=function(){//{{{
  this.titleW=document.createElement("div");
  this.titleW.className="mbFormTitle";
  this.widget.appendChild(this.titleW);
}//}}}
def.__createMiddleContent=function(){//{{{
  var middle=document.createElement("div");
  middle.className="mbFormMiddleContent";
  this.content.appendChild(middle);
  this.__createPreviousButton(middle);
  this.__createFormItem(middle)
  this.__createNextButton(middle);
}//}}}
def.__createPreviousButton=function(middle){//{{{
  this.previousB=document.createElement("button");
  this.previousB.className="mbFormButton";
  this.previousB.id="mbFormButtonPrevious";
  this.previousB.innerHTML="← ";
  this.previousB.onclick=this.__previous.bind(this);
  this.previousB.onmouseover=this.__showInfo.bind(this,"previous");
  this.previousB.onmouseleave=this.__hideInfo.bind(this);
  middle.appendChild(this.previousB)
}//}}}
def.__createNextButton=function(middle){//{{{
  this.nextB=document.createElement("button");
  this.nextB.className="mbFormButton";
  this.nextB.id="mbFormButtonNext";
  this.nextB.innerHTML="→";
  this.nextB.onclick=this.__next.bind(this);
  this.nextB.onmouseover=this.__showInfo.bind(this,"next");
  this.nextB.onmouseleave=this.__hideInfo.bind(this);
  middle.appendChild(this.nextB)
}//}}}
def.__showInfo=function(text){//{{{
  this.info.innerHTML=text;
}//}}}
def.__hideInfo=function(){//{{{
  this.info.innerHTML="";
}//}}}
def.__createInfoWidget=function(){//{{{
  this.info=document.createElement("div");
  this.info.className="mbFormInfo";
  this.content.appendChild(this.info);
}//}}}
def.__createFormItem=function(middle){//{{{
  this.formItem=document.createElement("div");
  this.formItem.className="mbFormItem";
  middle.appendChild(this.formItem);
}//}}}
def.__createCommentBar=function(){//{{{
  this.commentBar=document.createElement("div");
  this.commentBar.className="mbFormComment";
  this.content.appendChild(this.commentBar);
}//}}}
def.__createProgressBar=function(){//{{{
  var w=document.createElement("div");
  w.className="mbFormProgressBar";
  this.progress=document.createElement("div");
  this.progress.className="mbFormProgress";
  w.appendChild(this.progress);
  this.content.appendChild(w);
}//}}}
def.__previous=function(event){//{{{
  var obj={};
  obj._id=this.__currentID;
  obj.formName=this.__currentForm;
  this.mb.formCom.sendForPreviousFormItem(obj);
}//}}}
def.__next=function(event){//{{{
  var obj={};
  obj._id=this.__currentID;
  obj.formItem={
    label:this.__currentLabel,
    value:this.getFormItemValue()
  }
  obj.formName=this.__currentForm;
  this.mb.formCom.sendFormItem(obj);
}//}}}
def.show=function(){//{{{
  this.widget.style.visibility="visible";
}//}}}
def.hide=function(){//{{{
  this.widget.style.visibility="hidden";
}//}}}

def.getFormItemValue=function(){//{{{
  return this.__currentFormItem.value;
}//}}}
def.setTitle=function(title){//{{{
  this.titleW.innerHTML=title;
  this.__currentForm=title;
}//}}}
def.setFormItem=function(data){//{{{
  var w1=document.createElement("span"),
      w2=document.createElement("span"),
      w3;
  w1.innerHTML=data.label;
  w2.innerHTML=":";
  switch(data.type){
    case "entry":
      w3=document.createElement("input");
      w3.spellcheck=false;
      w3.innerHTML=data.value;
      break;
    case "password":
      w3=document.createElement("input");
      w3.spellcheck=false;
      w3.type="password";
      w3.innerHTML=data.value;
      break;
    case "select":
      w3=document.createElement("select");
      var o;
      data.options.forEach(function(option){
        o=document.createElement("option");
        o.innerHTML=option;
        w3.appendChild(o);
      })
      break;
  }
  this.formItem.innerHTML="";
  this.formItem.appendChild(w1);
  this.formItem.appendChild(w2);
  this.formItem.appendChild(w3);
  w3.onkeyup=this.__onkeyup.bind(this)
  w3.focus();
  this.__currentLabel=data.label;
  this.__currentFormItem=w3;
  return w3;
}//}}}
def.setData=function(data){//{{{
  this.__currentID=data._id;
  this.setTitle(data.title);
  this.__currentFormItem=this.setFormItem(data.item);
  this.setProgress(data.progress);
}//}}}
def.setComment=function(comment){//{{{
  this.commentBar.innerHTML=comment;
}//}}}
def.setProgress=function(data){//{{{
  this.progress.innerHTML="";
  var w;
  for(var i=0;i<data.amount;i++){
    w=document.createElement("div");
    if(i<=data.index){
      w.className="mbFormProgressDot mbFormProgressDotSelected";
    }else{
      w.className="mbFormProgressDot mbFormProgressDotDeselected";
    }
    this.progress.appendChild(w);
  }
}//}}}
def.__onkeyup=function(event){//{{{
  if(event.keyCode==13){
    this.__next(event);
  }
}//}}}
//}}}

function mbTable(mb,master){//{{{
  mbWidget.call(this,mb,master,"mbTable");
  this.prepare();
  this.__current=null;
  this.__currentClassName=null;
  this.__selected={
    color:"#000",
    background:"brown"
  }
}
var def=construct(mbWidget,mbTable);
def.prepare=function(){//{{{
  this.__createTitle();
  this.content=document.createElement("div");
  this.content.className="mbTableContent";
  this.__createTable();
  this.__createButtons();
  this.widget.appendChild(this.content);
}//}}}
def.__createTitle=function(){//{{{
  this.title=document.createElement("div");
  this.title.className="mbTableTitle";
  this.widget.appendChild(this.title);
}//}}}
def.__createTable=function(){//{{{
  this.table=document.createElement("div");
  this.table.className="mbTableTable";
  this.content.appendChild(this.table);
}//}}}
def.__createButtons=function(){//{{{
  this.removeB=document.createElement("div");
  this.removeB.className="mbTableButton";
  this.removeB.id="mbTableButtonRemove";
  this.removeB.innerHTML="-";
  this.addB=document.createElement("div");
  this.addB.className="mbTableButton";
  this.addB.id="mbTableButtonAdd";
  this.addB.innerHTML="+";
  this.editB=document.createElement("div");
  this.editB.className="mbTableButton";
  this.editB.id="mbTableButtonEdit";
  this.editB.innerHTML="✎";
  this.widget.appendChild(this.removeB)
  this.widget.appendChild(this.addB)
  this.widget.appendChild(this.editB)
}//}}}
def.setData=function(data){//{{{
  this.setTitle(data.title);
  this.setHeader(data.headers);
  this.setTable(data.rows);
}//}}}
def.setTitle=function(title){//{{{
  this.title.innerHTML=title;
}//}}}
def.setHeader=function(headers){//{{{
  var row=document.createElement("tr"),
      h;
  headers.forEach(function(header){
    h=document.createElement("td");
    h.innerHTML=header;
    h.style["text-transform"]="uppercase";
    h.style["text-decoration"]="underline";
    row.appendChild(h);
  })
  this.table.appendChild(row);
}//}}}
def.setTable=function(rows){//{{{
  let r,h;
  rows.forEach(function(row){
    r=document.createElement("tr");
    row.cells.forEach(function(cell){
      h=document.createElement("td");
      h.innerHTML=cell;
      r.appendChild(h);
    })
    r.addEventListener("click",this.__onclick.bind(this,r));
    this.table.appendChild(r);
  }.bind(this))
}//}}}
def.select=function(elem){//{{{
  this.__deselect();
  this.__current=elem;
  this.__currentStyle={
    background:elem.style.background,
    color:elem.style.color
  };
  this.__current.style.background=this.__selected.background;
  this.__current.style.color=this.__selected.color;
}//}}}
def.__deselect=function(){//{{{
  if(this.__current!==null){
    this.__current.style.background=this.__currentStyle.background;
    this.__current.style.color=this.__currentStyle.color;
    this.__current=null;
  }
}//}}}
def.__onclick=function(item){//{{{
  this.select(item);
}//}}}
//}}}
function mbListForm(mb,master){//{{{
  mbWidget.call(this,mb,master,"mbForm");
  this.content=null;
  this.prepare();
  this.__items=[];
}
var def=construct(mbWidget,mbListForm);
def.prepare=function(){//{{{
  this.__createTitle();
  this.content=document.createElement("div");
  this.content.className="mbFormContent";
  this.widget.appendChild(this.content)
  this.__createMiddleContent();
  this.__createButtons();
}//}}}
def.__createMiddleContent=function(){//{{{
  this.__middle=document.createElement("div");
  this.__middle.className="mbFormMiddleContent";
  this.content.appendChild(this.__middle);
}//}}}
def.__createTitle=function(){//{{{
  this.titleW=document.createElement("div");
  this.titleW.className="mbFormTitle";
  this.widget.appendChild(this.titleW);
}//}}}
def.__createButtons=function(){//{{{
  var buttons=document.createElement("div"),
      b1=document.createElement("button"),
      b2=document.createElement("button");
  buttons.className="mbListFormButtons";
  b1.innerHTML="decline";
  b1.addEventListener("click",this.__decline.bind(this))
  b2.innerHTML="accept";
  b2.addEventListener("click",this.__accept.bind(this))
  buttons.appendChild(b1);
  buttons.appendChild(b2);
  this.content.appendChild(buttons);
}//}}}
def.setTitle=function(title){//{{{
  this.titleW.innerHTML=title;
  this.__currentForm=title;
}//}}}
def.__createFormItem=function(data){//{{{
  var item=document.createElement("div"),
      w1=document.createElement("span"),
      w2=document.createElement("span"),
      w3;
  item.className="mbFormItem";
  item.style.display="block";
  w1.style.border="none";
  this.__middle.appendChild(item);
  w1.innerHTML=data.label;
  w2.innerHTML=":";
  switch(data.type){
    case "entry":
      w3=document.createElement("input");
      w3.spellcheck=false;
      w3.innerHTML=data.value;
      break;
    case "password":
      w3=document.createElement("input");
      w3.spellcheck=false;
      w3.type="password";
      w3.innerHTML=data.value;
      break;
    case "select":
      w3=document.createElement("select");
      var o;
      data.options.forEach(function(option){
        o=document.createElement("option");
        o.innerHTML=option;
        w3.appendChild(o);
      })
      break;
  }
  item.appendChild(w1);
  item.appendChild(w2);
  item.appendChild(w3);
  this.__items.push({
    item:JSON.parse(JSON.stringify(data)),
    widget:w3
  })
}//}}}
def.setItems=function(items){//{{{
  items.forEach(function(item){
    this.__createFormItem(item);
  }.bind(this))
}//}}}
def.setData=function(data){//{{{
  this.__items=[];
  this.__middle.innerHTML="";
  this.setTitle(data.title);
  this.setItems(data.items);
}//}}}
def.getData=function(){//{{{
  var result={};
  this.__items.forEach(function(item){
    result[item.item.label]=item.widget.value;
  })
  return result;
}//}}}
def.__accept=function(event){//{{{
  print("accept")
  print(this.getData())
}//}}}
def.__decline=function(event){//{{{
  print("decline")
}//}}}
//}}}
function mbClientbar(mb,master){//{{{
  mbWidget.call(this,mb,master,"mbClientbar");
  this.prepare();
}
var def=construct(mbWidget,mbClientbar);
def.prepare=function(){//{{{
  this.content=document.createElement("div");
  this.content.className="mbClientbarContent";
  this.__createOptionSlot();
  this.__createClientWidget();
  this.widget.appendChild(this.content);
}//}}}
def.__createOptionSlot=function(){//{{{
  this.optionSlot=document.createElement("div");
  this.optionSlot.className="mbClientbarOptionSlot";
  this.content.appendChild(this.optionSlot)
}//}}}
def.__createClientWidget=function(){//{{{
  this.clientWidget=document.createElement("div");
  this.clientWidget.className="mbClientbarClientWidget";
  this.content.appendChild(this.clientWidget)
}//}}}
def.setClient=function(client){//{{{
  this.clientWidget.innerHTML="👤 "+client.name+" "+client.surname;
  return this;
}//}}}
def.setOptions=function(options){//{{{
  var op;
  options.forEach(function(option){
    op=document.createElement("div");
    op.className="mbClientbarOption";
    op.innerHTML=option.name;
    this.optionSlot.appendChild(op);
  }.bind(this))
  return this;
}//}}}
//}}}
function mbDock(mb,master){//{{{
  this.__createIconName();
  mbWidget.call(this,mb,master,"mbDock");
  this.prepare();
}
var def=construct(mbWidget,mbDock);
def.__createIconName=function(){//{{{
  this.iconName=document.createElement("div");
  this.iconName.className="mbIconName"
  document.body.appendChild(this.iconName);
}//}}}
def.prepare=function(){//{{{
  this.content=document.createElement("div");
  this.content.className="mbDockContent";
  this.widget.appendChild(this.content);
}//}}}
def.addIcon=function(data){//{{{
  var icon=document.createElement("div");
  icon.className="mbDockIcon";
  icon.innerHTML=data.symbol;
  icon.addEventListener("mouseenter",this.__onenter.bind(this,data.name))
  icon.addEventListener("click",this.__onclick.bind(this,data))
  icon.addEventListener("mouseleave",this.__onleave.bind(this))
  this.content.appendChild(icon);
}//}}}
def.setData=function(data){//{{{
  data.icons.forEach(function(icon){
    this.addIcon(icon);
  }.bind(this))
  return this;
}//}}}
def.__onenter=function(text){//{{{
  this.iconName.innerHTML=text;
}//}}}
def.__onleave=function(event){//{{{
  this.iconName.innerHTML="";
}//}}}
def.__onclick=function(icon){//{{{
  print(icon)
}//}}}

//}}}
function mbDesk(mb,master){//{{{
  //mbWidget.call(this,mb,master,"mbDesk")
  this.prepare();
}
var def=mbDesk.prototype;
def.prepare=function(){
  this.widget=document.getElementById("mbSVG");
  document.body.appendChild(this.widget);
}
def.drawPoint=function(data){//{{{
  var html="<circle id='"+data._id+"' r=40 cx="+data.x+" cy="+data.y+" \
    stroke='brown' stroke-width='1' fill='orange' />"
  this.widget.innerHTML+=html;
  return this;
}//}}}
def.drawPointer=function(data){//{{{
  var html='<line x1="0" y1="0" x2="200" y2="200" style="stroke:rgb(255,0,0);stroke-width:2" />'
  this.widget.innerHTML+=html;
  return this;
}//}}}
def.drawPointers=function(data){//{{{
}//}}}
def.drawSegment=function(data){//{{{
}//}}}
def.drawPage=function(data){//{{{
  var html="<path stroke='"+data.stroke+"' stroke-width=1 fill='"+data.fill+"' d='"
  var path="M"+data.x1+" "+data.y1+" "+data.x2+" "+data.y2+" "+data.x3+" "+data.y3+" "+data.x4+" "+data.y4+" Z";
  html+=path+"'></path>"
  this.widget.innerHTML+=html;
  return this;
}//}}}


//}}}

function mbChatWindow(mb,master){//{{{
  mbWidget.call(this,mb,master,"mbChatWindow");
  this.prepare();
}
var def=construct(mbWidget,mbChatWindow);
def.prepare=function(){//{{{
  this.createTitle();
  this.createContent();
  this.createEntry();
}//}}}
def.createTitle=function(){//{{{
  this.title=document.createElement("div");
  this.title.className="mbChatWindowTitle";
  this.widget.appendChild(this.title);
}//}}}
def.createContent=function(){//{{{
  this.content=document.createElement("div");
  this.content.className="mbChatWindowContent";
  this.widget.appendChild(this.content);
}//}}}
def.createEntry=function(){//{{{
  var w=document.createElement("div");
  w.className="mbChatWindowEntry";
  this.entry=document.createElement("input");
  this.button=document.createElement("button");
  this.button.innerHTML="send";
  w.appendChild(this.entry);
  w.appendChild(this.button);
  this.widget.appendChild(w);
}//}}}
def.setData=function(data){//{{{
  this.setTitle(data.title);
  this.setMessages(data.messages);
  return this;
}//}}}
def.setTitle=function(title){//{{{
  this.title.innerHTML=title;
  return this;
}//}}}
def.setMessages=function(messages){//{{{
  this.content.innerHTML="";
  var w;
  messages.forEach(function(message){
    w=document.createElement("div");
    w.innerHTML=message.text;
    if(message.isAuthor){
      w.className="mbChatWindowMessage mbChatWindowMessageSender";
    }else{
      w.className="mbChatWindowMessage mbChatWindowMessageReceiver";
    }
    this.content.appendChild(w);
  }.bind(this))
  return this;
}//}}}
def.__onclick=function(event){//{{{
}//}}}
def.__send=function(event){//{{{

}//}}}
def.__cleanEntry=function(){//{{{
  this.entry.value="";
}//}}}
def.show=function(){//{{{
}//}}}
def.hide=function(){//{{{
}//}}}
def.destroy=function(){//{{{
}//}}}
//}}}
function mbChatManager(mb,master){//{{{
  mbWidget.call(this,mb,master,"mbChatMenu");
  this.contactsVisible=false;
  this.prepare();
}
var def=construct(mbWidget,mbChatManager);
def.prepare=function(){//{{{
  this.contactList=document.createElement("div");
  this.contactList.className="mbContactList";
  this.contactButton=document.createElement("div");
  this.contactButton.className="mbContactButton";
  this.contactButton.innerHTML=">>";
  this.contactButton.addEventListener("click",this.__clickContactButton.bind(this))
  this.chatSlot=document.createElement("div");
  this.chatSlot.className="mbChatSlot";
  this.widget.appendChild(this.contactList);
  this.widget.appendChild(this.contactButton);
  this.widget.appendChild(this.chatSlot);
}//}}}
def.openChatWindow=function(data){//{{{
  var chat=new mbChatWindow(this.mb,this.chatSlot);
  chat.setData(data);
  return chat;
}//}}}
def.closeChatWindow=function(data){//{{{
}//}}}
def.setContacts=function(contacts){//{{{
  var c;
  this.contactList.innerHTML="";
  contacts.forEach(function(contact){
    c=document.createElement("div");
    c.className="mbContact";
    c.innerHTML=contact.name+" "+contact.surname;
    this.contactList.appendChild(c);
  }.bind(this))
  return this;
}//}}}
def.__clickContactButton=function(event){//{{{
  if(this.contactsVisible){
    this.hideContacts();
  }else{
    this.showContacts();
  }
}//}}}
def.showContacts=function(){//{{{
  this.__setContactsVisibility(true);
  this.contactList.style.width=140;
  this.contactButton.innerHTML="<<";
}//}}}
def.hideContacts=function(){//{{{
  this.__setContactsVisibility(false);
  this.contactList.style.width=0;
  this.contactButton.innerHTML=">>";
}//}}}
def.__setContactsVisibility=function(visibility){//{{{
  this.contactsVisible=visibility;
}//}}}
//}}}

function mbOptionBar(mb,master){//{{{
  mbWidget.call(this,mb,master,"mbOptionBar");
  this.prepare();
}
var def=construct(mbWidget,mbOptionBar);
def.prepare=function(){//{{{
}//}}}
def.setOptions=function(options){//{{{
  var w=document.createElement("div");
  options.forEach(function(option){
    w.className="mbOption";

  }.bind(this))
}//}}}
//}}}

function mbToolMenu(mb,master){//{{{
  mbWidget.call(this,mb,master,"mbToolMenu");
}
var def=construct(mbWidget,mbToolMenu);

//}}}

function mbDrawer(mb,master){//{{{
  mbWidget.call(this,mb,master,"mbDrawer");
}
var def=construct(mbWidget,mbDrawer);
//}}}

function mbNotification(mb,master){//{{{
  mbWidget.call(this,mb,master,"mbNotification");
}
var def=construct(mbWidget,mbNotification);
def.prepare=function(){//{{{
  this.__createTitle();
  this.__createContent();
  this.createButtons();
}//}}}
def.__createTitle=function(){//{{{
  this.title=document.createElement("div");
  this.title.className="mbNotificationTitle";
  this.widget.appendChild(this.title);
}//}}}
def.__createContent=function(){//{{{
  this.content=document.createElement("div");
  this.content.className="mbNotificationContent";
  this.widget.appendChild(this.content);
}//}}}
def.createButtons=function(){//{{{
  this.buttons=document.createElement("div");
  this.buttons.className="mbNotificationButtons";
  this.widget.appendChild(this.buttons);
}//}}}
def.setTitle=function(title){//{{{
  this.title.innerHTML=title;
  return this;
}//}}}
def.setText=function(text){//{{{
  this.content.innerHTML=text;
  return this;
}//}}}
def.setData=function(data){//{{{
  this.setTitle(data.title);
  this.setText(data.text);
  
  return this;
}//}}}
//}}}
function mbInfoNotification(mb,master){//{{{
  mbNotification.call(this,mb,master);
  this.prepare();
}
var def=construct(mbNotification,mbInfoNotification);
def.prepare=function(){//{{{
  mbNotification.prototype.prepare.call(this);
  this.createButton();
}//}}}
def.createButton=function(){//{{{
  this.okButton=document.createElement("button");
  this.okButton.addEventListener("click",this.__okClick.bind(this));
  this.okButton.innerHTML="ok";
  this.buttons.appendChild(this.okButton);
  return this;
}//}}}
def.__okClick=function(event){//{{{
  print("ok notification")
}//}}}
def.setData=function(data){//{{{
  mbNotification.prototype.setData.call(this,data);
}//}}}
//}}}
function mbNotificationManager(mb,master){//{{{
  mbWidget.call(this,mb,master,"mbNotificationSlot");
}
var def=construct(mbWidget,mbNotificationManager);
def.prepare=function(){//{{{

}//}}}

def.addInfoNotification=function(data){
  var w=new mbInfoNotification(this.mb,this.widget);
  w.setData(data);
  return w;
}
//}}}
//COMMANDS{{{
function mbCommand(mb,name){
  this.mb=mb;
  this.name=name;
}
var def=mbCommand.prototype;
def.run=function(data){//{{{
  print("command "+this.name+" is not implemented.");
}//}}}
function mbSetClientBarOptionsCommand(mb){
  mbCommand.call(this,mb,"clientbar.options.set")
}
var def=construct(mbCommand,mbSetClientBarOptionsCommand);
def.run=function(data){//{{{
  this.mb.clientbarW.setData(data.options);
}//}}}
//}}}

//API{{{
function API(mb){
  this.mb=mb;
  this.__commandSL={};
  this.__commands=[];
}
var def=API.prototype;
def.addCommand=function(commandClass){//{{{
  var command=new commandClass(this.mb);
  this.__commandSL[command.name]=command;
  this.__commands.push(command);
  return this;
}//}}}
def.addCommands=function(commands){//{{{
  commands.forEach(function(commandClass){
    this.addCommand(commandClass);
  }.bind(this));
  return this;
}//}}}
def.getCommand=function(name){//{{{
  return this.__commandSL[name];
}//}}}
def.runCommand=function(name,data){//{{{
  this.getCommand(name).run(data);
}//}}}
def.runCommands=function(names,data){//{{{
  names.forEach(function(name){
    this.runCommand(name,data);
  }.bind(this))
}//}}}
def.showCommands=function(){//{{{
  this.__commands.forEach(function(command){
    print(command.name);
  }.bind(this))
  return this;
}//}}}
//API}}}

//TESTS{{{
function Test(api){
  this.api=api;
}
var def=Test.prototype;
//}}}



function mbMain(){//{{{
  this.deskW=new mbDesk(this,null);
  this.clientbarW=new mbClientbar(this,null);

  this.toolM=new mbToolMenu(this,null);

  this.dockW=new mbDock(this,this.toolM.widget);
  this.optionbarW=new mbOptionBar(this,this.toolM.widget);
  this.drawer=new mbDrawer(this,this.toolM.widget);
  this.notificationM=new mbNotificationManager(this,null);
  
  this.formCom=new mbFormCommunication(this,socket);
  this.formW=new mbForm(this,null);
  this.formCom.startForm("login");

  this.tableW=new mbTable(this,null);
  this.chatM=new mbChatManager(this,null);

  this.api=new API(this);
  this.api.addCommands([
    mbSetClientBarOptionsCommand
  ]).showCommands();
  
  /*
  this.notificationM.addInfoNotification({
    title:"account creation",
    text:"You have successfully created your account",
    type:"info"
  });
  this.notificationM.addInfoNotification({
    title:"new tools",
    text:"We, have added new tool to your toolbox, check it out.",
    type:""
  });
*/
  this.dockW.setData({
    icons:[{
      name:"settings",
      symbol:"⚙"
    },{
      name:"backlog",
      symbol:"⛏"
    },{
      name:"friends",
      symbol:"👤"
    },{
      name:"draw",
      symbol:"🖍"
    }],
  })
  this.clientbarW.setClient({
    name:"marek",
    surname:"bibulski"
  }).setOptions([{
    name:"log in"
  },{
    name:"sign in"
  }])
  
  /*

  this.deskW.drawPoint({
    _id:"point01",
    x:600,
    y:60
  }).drawPointer({
    _id:"pointer01",
    x:600,
    y:500
  }).drawPage({
    x1:650.5,
    y1:50.5,
    x2:860.5,
    y2:50.5,
    x3:860.5,
    y3:347.5,
    x4:650.5,
    y4:347.5,
    fill:"#fffff0",
    type:"blank",
    stroke:"#000"
  })
  */
  /*
  this.chatM.openChatWindow({
    title:"kate bush",
    messages:[{
      isAuthor:true,
      text:"hej",
      time:""
    },{
      isAuthor:false,
      text:"no hej",
      time:""
    },{
      isAuthor:true,
      text:"co tam?",
      time:""
    },{
      isAuthor:false,
      text:"jakis dlugi tekst zeby sprawdzic czy jest ok"
    },{
      isAuthor:true,
      text:"co tam?",
      time:""
    },{
      isAuthor:false,
      text:"jakis dlugi tekst zeby sprawdzic czy jest ok"
    }]
  });

  this.chatM.openChatWindow({
    _id:"conversation_01",
    title:"marco polo",
    messages:[]
  })
  this.chatM.openChatWindow({
    title:"marco polo",
    messages:[]
  })
  this.chatM.openChatWindow({
    title:"marco polo",
    messages:[]
  })
  this.chatM.openChatWindow({
    title:"marco polo",
    messages:[]
  })
  this.chatM.setContacts([{
    name:"kate",
    surname:"bush"
  },{
    name:"marco",
    surname:"polo"
  },{
    name:"agnieszka",
    surname:"hawryłko"
  }])
*/
  //tests{{{
  this.tableW.setData({
    title:"table 1",
    rows:[{
      cells:["raz","dwa","trzy"]
    },{
      cells:["cztery","piec","szesc"]
    },{
      cells:["raz","dwa jakis dlugi tekst","trzy"]
    },{
      cells:["cztery","piec","szesc"]
    }],
    headers:["column1","column2","column3"]
  });

  this.listForm=new mbListForm(this,null);
  this.listForm.setData({
    title:"list form",
    items:[{
      label:"item1",
      type:"entry",
      value:"",
      options:[]
    },{
      label:"item2",
      type:"entry",
      value:"",
      options:[]
    },{
      label:"item3",
      type:"select",
      value:"",
      options:["marco","polo"]
    }]
  })
  //}}}
  
  //this.formW.setComment("jakis błąd wynikł....")
}
var def=mbMain.prototype;
//}}}
window.onload=function(){//{{{
  var mb=new mbMain();
}//}}}
