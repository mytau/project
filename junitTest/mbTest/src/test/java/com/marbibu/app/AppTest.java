package com.marbibu.app;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.regex.*;

public class AppTest{
  @Test
  public void mailIsCorrect(){//{{{
    App data=new App();
    data.register("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa");
    Pattern a=Pattern.compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
    Matcher m=a.matcher(data.mail);
    System.out.println(m);
    assertTrue(m.matches());
  }//}}}
  @Test
  public void mailIsIncorrect(){//{{{
    App data=new App();
    data.register("marco","polo","marco.polo.wrong",11111111,"aaa","aaa");
    Pattern a=Pattern.compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
    Matcher m=a.matcher(data.mail);
    System.out.println(m);
    assertFalse(m.matches());
  }//}}}
  @Test
  public void passwordsMatches(){//{{{
    App data=new App();
    data.register("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa");
    assertEquals(data.password,data.confirmPassword);
  }//}}}
  @Test
  public void nameIsNotEmpty(){//{{{
    App data=new App();
    data.register("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa");
    System.out.println(data.name.equals(""));
    assertFalse(data.name.equals(""));
  }//}}}
  @Test
  public void surnameIsNotEmpty(){//{{{
    App data=new App();
    data.register("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa");
    assertFalse(data.surname.equals(""));
  }//}}}
  @Test
  public void passwordIsNotEmpty(){//{{{
    NodeJSEcho client=new NodeJSEcho();
    App data=new App();
    data.register("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa");
    assertFalse(data.password.equals(""));
  }//}}}
}
