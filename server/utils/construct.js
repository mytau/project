'use strict'
var construct=function(parent,child){//{{{
  child.prototype=Object.create(parent.prototype);
  child.prototype.constructor=child;
  return child.prototype;
}//}}}
module.exports=construct;
