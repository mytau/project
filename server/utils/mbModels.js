'use strict'
//IMPORTS{{{
var print=require("./print"),
    construct=require("./construct");
//}}}
//=====================
function mbSystem(mb,name){//{{{
  this.mb=mb;
  this.name=name;
}
var def=mbSystem.prototype;
//}}}
function mbManager(mb,db,name){//{{{
  this.mb=mb;
  this.__db=db;
  this._name=name;
  this.objID=this.__db.Schema.ObjectId;
  this.__model=null;
}
var def=mbManager.prototype;
def._newModel=function(doc){//{{{
  return new Promise((resolve,reject)=>{
    if(this.mb.SETTINGS.client.test.display==="CTO"){
      print(`--> db: model ${this._name} was created: ${JSON.stringify(data)}.`);
    }
    resolve(doc);
  })
}//}}}
def.getModelWith=function(condition){//{{{
  return this.__model.findOne(condition).exec();
}//}}}
def.getModelWith_query=function(condition){//{{{
  return this.__model.findOne(condition);
}//}}}
def.updateModelDataWith=function(condition,data){//{{{
  return this.__model.findOneAndUpdate(
    condition,
    {"$set":data},
    {new:true}
  ).exec();
}//}}}
def.delModelWith=function(condition){//{{{
  return this.__model.findOneAndRemove(condition).exec();
}//}}}
def.getModelData_query=function(condition,dataString){//{{{
  return this.__model.findOne(condition,dataString);
}//}}}
def.getModelData=function(condition,dataString){//{{{
  return this.getModelData_query(condition,dataString).exec();
}//}}}
def.delModelWith=function(condition){//{{{
  /*return this.__model.findOneAndRemove(condition).exec().then(doc=>{
    print(`--> db: model ${this._name} was removed by ${JSON.stringify(condition)}.`);
    return doc;
  });*/
  return this.__model.find(condition).remove().exec().then(doc=>{
    //print(`--> db: model ${this._name} was removed by ${JSON.stringify(condition)}.`);
    return doc;
  });
}//}}}
def.getModelsData_query=function(condition,dataString){//{{{
  return this.__model.find(condition,dataString);
}//}}}
def.delAllModels=function(){//{{{
  return this.__model.remove({}).exec().then(doc=>{
    //print(`--> db: collection of model ${this._name} is empty now.`);
    return doc;
  })
}//}}}
def.showModels=function(){//{{{
  return this.__model.find({}).exec().then(docs=>{
    docs.forEach(doc=>{
      //print(JSON.stringify(doc));
    })
  })
}//}}}
def.hasModelWith=function(condition){//{{{
  return this.getModelWith(condition)
    .then(doc=>{
      if(doc!==null){
        return true;
      }
      return false;
    })
}//}}}
def.getModels_query=function(condition){//{{{
  return this.__model.find(condition);
}//}}}
def.getModels=function(condition){//{{{
  return this.getModels_query(condition).exec();
}//}}}
def.getModelsExcept=function(condition,except){//{{{
  return this.getModelsExcept_query(condition,except).exec();
}//}}}
def.getModelsExcept_query=function(condition,except){//{{{
  let obj=Object.assign(condition,except)
  return this.__model.find(obj);
}//}}}
//except
//key:{$nin:[_id]}
//}}}
function mbCommand(mb,data){//{{{
  this.mb=mb;
  this.data=data;
  this.data.args=this.__prepareArgs();
}
var def=mbCommand.prototype;
def.__prepareArgs=function(){//{{{
  let result=[];
  this.data.args.forEach(arg=>{
    if(typeof(arg)==="object"){
      result.push(arg);
    }else{
      result.push(new arg(this.mb));
    }
  })
  return result;
}//}}}
def.validateSync=function(CTO){//{{{
  let i,validator=null,result;
  for(i=0;i<CTO.command.args.length;i++){
    result=this.data.args[i].validateSync(CTO,CTO.command.args[i])
    if(result!==null){
      return result;
    }
  }
  return null;
}//}}}
def.validateAsync=function(CTO){//{{{
  let result=Promise.resolve();
  return new Promise((resolve,reject)=>{
    resolve();
  }).then(doc=>{
    this.data.args.map((arg,index)=>{
      result=result.then((doc)=>{
        if(doc){
          return arg.validateAsync(CTO,CTO.command.args[index])
        }else{
          return 
        }
      })
    })
    return result;
  }).catch(err=>{
    CTO.data.comment=err.comment;
    CTO.data.bool=false;
    return err;
  })
}//}}}
def.getArgumentsObject=function(values){//{{{
  let result={},
      n=values.length;
  this.data.args.forEach((arg,index)=>{
    if(index>n-1){
      result[arg.name]=null;
    }else{
      result[arg.name]=values[index];
    }
  })
  return result;
}//}}}
def.validateArguments=function(CTO){//{{{
  return new Promise((resolve,reject)=>{
    resolve();
  }).then(doc=>{
    return {
      comment:"",
      bool:true
    }
  })
}//}}}
def._run=function(socket,data,cb,bool){//{{{
  let cto=null,
      command;
  return this.mb.getManager("Client").genCTO(socket,data,bool)
    .then(CTO=>{
      cto=CTO;
      if(this.mb.getSystem("Role").hasRole(this.data.roles,cto.client.role)){
        command=this;
      }else{
        command=this.mb.getSystem("Command").getCommand("system","error","noPermission");
        cto.command.args=[];
      }
    }).then(doc=>{
      let validator=command.validateSync(cto);
      return new Promise((resolve,reject)=>{
        if(validator){
          cto.data.bool=false;
          cto.data.comment=validator.comment;
          command=this.mb.getSystem("Command").getCommand("system","error","validationFail")
          reject(cto);
        }else{
          cto.data.comment=command.data.comment;
          resolve(cto);
        }
      })
    }).then(doc=>{
      return command.validateArguments(cto)
    }).then(doc=>{
      if(doc.bool){
        return command.run(cto);
      }else{
        command=this.mb.getSystem("Command").getCommand("system","error","validationFail")
        cto.data.bool=false;
        cto.command.args=[doc.comment];
        return command.run(cto);
      }
    }).then(doc=>{
      cto.data.result=doc;
      if(cb){
        //print(JSON.stringify(cto,undefined,2));
        cb(cto);
      }else{
        command.mb.sendData(cto);
      }
      return cto;
    })
    .catch(err=>{
      if(cb){
        cb(cto);
        return true;
      }else{
        return command.mb.sendData(cto);
      }
    })
}//}}}
def.run=function(CTO){//{{{
  print("run not implemented");
}//}}}
//}}}
//VALIDATORS{{{
function mbValidator(mb,comment){//{{{
  this.mb=mb;
  this.comment=comment;
}
var def=mbValidator.prototype;
def._validate=function(CTO,value){//{{{
  let result=false
  if(this.validate(CTO,value)){
    result=true;
  }
  return result;
}//}}}
def.validate=function(CTO,value){//{{{
  return true;
}//}}}
//}}}
function mbMailValidator(mb){//{{{
  mbValidator.call(this,mb,"Wrong mail address.")
}
var def=construct(mbValidator,mbMailValidator);
def.validate=function(CTO,value){//{{{
  let re=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let result=re.test(value);
  return result
}//}}}
//}}}
function mbIsNotEmptyStringValidator(mb){//{{{
  mbValidator.call(this,mb,"Empty string given.")
}
var def=construct(mbValidator,mbIsNotEmptyStringValidator);
def.validate=function(CTO,value){//{{{
  return (value!=="");
}//}}}
//}}}
function mbIsFloatValidator(mb){//{{{
  mbValidator.call(this,mb,"It's not a float value.")
}
var def=construct(mbValidator,mbIsFloatValidator);
def.validate=function(CTO,value){//{{{
  let val=parseFloat(value);
  return (!isNan(val));
}//}}}
//}}}
function mbAsyncValidator(mb,comment){//{{{
  this.mb=mb;
  this.comment=comment;
}
var def=mbAsyncValidator.prototype;
def._validate=function(CTO,value,data){//{{{
    return this.validate(CTO,value)
      .then(doc=>{
        return new Promise((resolve,reject)=>{
          if(doc){
            reject(this);
          }else{
            resolve();
          }
        })
      })
}//}}}
def.validate=function(CTO,value){//{{{
  return new Promise((resolve,reject)=>{
    resolve();
  })
}//}}}
//}}}
function mbClientHasMailValidator(mb){//{{{
  mbAsyncValidator.call(this,mb,"Such mail already exists.")
}
var def=construct(mbAsyncValidator,mbClientHasMailValidator);
def.validate=function(CTO,value){//{{{
  return this.mb.getManager("Client").getModelWith({
    mail:value
  })
}//}}}
//}}}
//VALIDATORS}}}
//ARGS{{{
function mbArg(mb,validators,asyncValidators,name){//{{{
  this.name=name;
  this.mb=mb;
  this.__validators=this.__prepareValidators(validators);
  this.__asyncValidators=this.__prepareValidators(asyncValidators);
}
var def=mbArg.prototype;
def.__prepareValidators=function(validators){//{{{
  let result=[];
  validators.forEach(validatorClass=>{
    result.push(new validatorClass(this.mb))
  })
  return result;
}//}}}
def.validateSync=function(CTO,value){//{{{
  let i=0,
      validator=null;
  for(i=0;i<this.__validators.length;i++){
    validator=this.__validators[i]
    if(!validator._validate(CTO,value)){
      return validator;
    }
  }
  return null;
}//}}}
def.validateAsync=function(CTO,value){//{{{
  let result=Promise.resolve();
  return new Promise((resolve,reject)=>{
    resolve();
  }).then(doc=>{
    this.__asyncValidators.map(valid=>{
      result=result.then((doc)=>valid._validate(CTO,value))
    })
    return result;
  })
}//}}}
//}}}
function mbArg(mb,validators,asyncValidators,name){//{{{
  this.name=name;
  this.mb=mb;
  this.__validators=this.__prepareValidators(validators);
  this.__asyncValidators=this.__prepareValidators(asyncValidators);
}
var def=mbArg.prototype;
def.__prepareValidators=function(validators){//{{{
  let result=[];
  validators.forEach(validatorClass=>{
    result.push(new validatorClass(this.mb))
  })
  return result;
}//}}}
def.validateSync=function(CTO,value){//{{{
  let i=0,
      validator=null;
  for(i=0;i<this.__validators.length;i++){
    validator=this.__validators[i]
    if(!validator._validate(CTO,value)){
      return validator;
    }
  }
  return null;
}//}}}
def.validateAsync=function(CTO,value){//{{{
  let result=Promise.resolve();
  return new Promise((resolve,reject)=>{
    resolve();
  }).then(doc=>{
    this.__asyncValidators.map(valid=>{
      result=result.then((doc)=>valid._validate(CTO,value))
    })
    return result;
  })
}//}}}
//}}}
function mbMailArg(mb,name){//{{{
  mbArg.call(this,mb,[
    mbMailValidator
  ],[
    mbClientHasMailValidator
  ],name);
}
var def=construct(mbArg,mbMailArg);

//}}}
function mbMailSimpleArg(mb,name){//{{{
  mbArg.call(this,mb,[
    mbMailValidator
  ],[
  ],name);
}
var def=construct(mbArg,mbMailSimpleArg);

//}}}
function mbTextArg(mb,name){//{{{
  mbArg.call(this,mb,[
    mbIsNotEmptyStringValidator
  ],[],name);
}
var def=construct(mbArg,mbTextArg);
//}}}
function mbFloatArg(mb,name){//{{{
  mbArg.call(this,mb,[
    mbIsNotEmptyStringValidator
  ],[],name);
}
var def=construct(mbArg,mbFloatArg);
//}}}
//ARGS}}}
function mbScenario(mb,module,name,description){//{{{
  this.mb=mb;
  this.module=module;
  this.name=name;
  this.description=description;
}
var def=mbScenario.prototype;
def.run=function(){//{{{
  //ma zwrócić Promise
}//}}}
//}}}
function mbForm(data){//{{{
  this.name=data.name;
  this.__data=data;
}
var def=mbForm.prototype;
def.getObject=function(){//{{{
  return this.__data;
}//}}}
//}}}


//trzeba zrobic cos do tworzenia progressu
//


module.exports={//{{{
  mbManager:mbManager,
  mbCommand:mbCommand,
  mbSystem:mbSystem,
  mbScenario:mbScenario,
  mbValidator:mbValidator,
  mbAsyncValidator:mbAsyncValidator,
  mbArg:mbArg,
  mbTextArg:mbTextArg,
  mbFloatArg:mbFloatArg,
  mbMailArg:mbMailArg,
  mbMailSimpleArg:mbMailSimpleArg,
  mbScenario:mbScenario,
  mbForm:mbForm
}//}}}
