var chai=require("chai"),
    expect=require("chai").expect;

function RegisterUser(name,surname,mail,mobile,password,confirmPassword){//{{{
  this.name=name;
  this.surname=surname,
  this.mail=mail;
  this.mobile=mobile;
  this.password=password;
  this.confirmPassword=confirmPassword;
  this.resData=null;
}
var def=RegisterUser.prototype;
def.getDataObject=function(){//{{{
  return {
    name:this.name,
    surname:this.surname,
    mail:this.mail,
    mobile:this.mobile,
    password:this.password,
    confirmPassword:this.confirmPassword
  }
}//}}}
def.register=function(socket){//{{{
  var obj=JSON.stringify({
    name:"user.basic.registerUser",
    args:this.getDataObject(),
    respond:{
      repond:"object",
      push:"object",
      type:"developer"
    }
  });
  socket.emit("sendData",obj,function(res){
    this.resData=res;
    this.getResData()
  }.bind(this))
}//}}}
def.register2=function(socket){//{{{
  var obj=JSON.stringify({
    name:"user.basic.registerUser2",
    args:this.getDataObject(),
    respond:{
      repond:"object",
      push:"object",
      type:"developer"
    }
  });
  socket.emit("sendData",obj,function(res){
    this.resData=res;
    this.getResData()
  }.bind(this))
}//}}}
def.register3=function(socket){//{{{
  var obj=JSON.stringify({
    name:"user.basic.logOut",
    args:this.getDataObject(),
    respond:{
      repond:"object",
      push:"object",
      type:"developer"
    }
  });
  socket.emit("sendData",obj,function(res){
    this.resData=res;
    this.getResData()
  }.bind(this))
}//}}}
def.getResData=function(){//{{{
  return this.resData;
}//}}}
//}}}

describe("RegisterUser",function(){
  var io= require('socket.io-client');
  var socket=null;
  var data=null;
  socket=io.connect("http://localhost:5002",{reconnect:true});
  
  it("correct mail",function(){//{{{
    console.log("checking: correct mail; ok");
    data=new RegisterUser("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa")
      var __re=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    result=__re.test(data.mail);
    expect(result).to.equal(true)
  })//}}}
  it("wrong mail",function(){//{{{
    console.log("checking: wrong mail; ok");
    data=new RegisterUser("marco","polo","marco.polo.wrong",11111111,"aaa","aaa")
      var __re=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    result=__re.test(data.mail);
    expect(result).to.not.equal(true)
  })//}}}
  it("passwords match each other",function(){//{{{
    console.log("checking: passwords match each other; ok");
    data=new RegisterUser("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa")
      expect(data.password).to.equal(data.confirmPassword)
  })//}}}
  it("name is not empty",function(){//{{{
    console.log("checking: name is not empty; ok");
    data=new RegisterUser("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa")
      expect(data.name).not.to.equal("")
  })//}}}
  it("surname is not empty",function(){//{{{
    console.log("checking: surname is not empty; ok");
    data=new RegisterUser("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa")
      expect(data.name).not.to.equal("")
  })//}}}
  it("not all pools were filled",function(done){//{{{
    data=new RegisterUser("","polo","marco.polo@gmail.com",11111111,"aaa","aaa")
    data.register(socket);
    setTimeout(function(){
      setTimeout(function(){
        expect(data.resData.data.comment).to.equal("You have not sent all data for registration.")
        console.log("comment: "+data.resData.data.comment+"; as expected");
        done();
      },1000)
    })
  })//}}}
  it("mail is not correct",function(done){//{{{
    data=new RegisterUser("marco","polo","marco.polo.wrong",11111111,"aaa","aaa")
    data.register(socket);
    setTimeout(function(){
      setTimeout(function(){
        expect(data.resData.data.comment).to.equal("Such mail is not correct.")
        console.log("comment: "+data.resData.data.comment+"; as expected");
        done();
      },1000)
    })
  })//}}}
  it("passwords are not equal",function(done){//{{{
    data=new RegisterUser("kate","bush","kate.bush@gmail.com",11111111,"aaa","bbb")
    data.register(socket);
    setTimeout(function(){
      setTimeout(function(){
        expect(data.resData.data.comment).to.equal("Introduced passwords are not similar.")
        console.log("comment: "+data.resData.data.comment+"; as expected");
        done();
      },1000)
    })
  })//}}}
  it("mail is not unique",function(done){//{{{
    data=new RegisterUser("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa")
    data.register(socket);
    setTimeout(function(){
      setTimeout(function(){
        expect(data.resData.data.comment).to.equal("Such mail is not unique.")
        console.log("comment: "+data.resData.data.comment+"; as expected");
        done();
      },1000)
    })
  })//}}}
  it("There is no such command",function(done){//{{{
    data=new RegisterUser("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa")
    data.register2(socket);
    setTimeout(function(){
      setTimeout(function(){
        expect(data.resData.data.comment).to.equal("No such command as user.basic.registerUser2")
        console.log("comment: "+data.resData.data.comment+"; as expected");
        done();
      },1000)
    })
  })//}}}
  it("You ave no permission to run this command",function(done){//{{{
    data=new RegisterUser("marco","polo","marco.polo@gmail.com",11111111,"aaa","aaa")
    data.register3(socket);
    setTimeout(function(){
      setTimeout(function(){
        expect(data.resData.data.comment).to.equal("You have no permission to run 'user.basic.logOut' command.")
        console.log("comment: "+data.resData.data.comment+"; as expected");
        done();
      },1000)
    })
  })//}}}
  it("You have succesfully registered user",function(done){//{{{
    data=new RegisterUser("kate","bush","kate.bush@gmail.com",11111111,"bbb","bbb")
    data.register(socket);
    setTimeout(function(){
      setTimeout(function(){
        expect(data.resData.data.comment).to.equal("You have successfully registered user.")
        console.log("comment: "+data.resData.data.comment+"; as expected");
        done();
      },1000)
    })
  })//}}}

/*  
  it('',function(){
    
  })*/
})
