'use strict'
function print(text){//{{{
  console.log(text);
}//}}}
var construct=function(parent,child){//{{{
  child.prototype=Object.create(parent.prototype);
  child.prototype.constructor=child;
  return child.prototype;
}//}}}

function mbCommandCenter(mb,socket){//{{{
  this.mb=mb;
  this.__socket=socket;
  this.__prepareSocketIO();
}
var def=mbCommandCenter.prototype;
def.__prepareSocketIO=function(){//{{{
  this.__socket.on("command",function(data){
    print(data);
  })
  this.__socket.on("kickOff",function(data){
    print(data);
  })
  this.__socket.on("order",function(tasks){
    this.sendListOfTasks(tasks)
  }.bind(this))
  this.__socket.on("fromOtherUser",function(data){
    print(data)
    this.mb.viewC.updateViews(data.view);
  }.bind(this))
  this.__socket.on("chatCommunication",function(data){
    print(JSON.stringify(data))
    document.getElementById("testPushSlot").innerHTML="you sent: "+data.text;
  })
}//}}}
def.sendListOfTasks=function(tasks){//{{{
  var p=Promise.resolve();
  tasks.map(function(task){
    p=p.then(function(){
      print(task.name)
      return this.sendDataPromise(task.name,task.args,task.respond,function(res){
        print(res);
      }.bind(this));
    }.bind(this))
  }.bind(this));
  return p;
}//}}}
def.sendData=function(name,args,respond,cb){//{{{
  var obj=JSON.stringify({
    name:name,
    args:args,
    respond:respond
  });
  var a=this.__socket.emit("sendData",obj,cb);
  print(a)
}//}}}
def.sendDataPromise=function(name,args,respond,cb){//{{{
  var obj=JSON.stringify({
    name:name,
    args:args,
    respond:respond
  });
  return new Promise(function(resolve,reject){
    resolve();
  }).then(function(doc){
    return this.__socket.emit("sendData",obj,cb)
  }.bind(this))
}//}}}
def.sendData2=function(obj,cb){//{{{
  this.__socket.emit("sendData",JSON.stringify(obj),cb)
}//}}}
//}}}
function mbViewCenter(mb){//{{{
  this.mb=mb;
}
var def=mbViewCenter.prototype;
def.updateView=function(view){//{{{
  if(view.length===undefined){
    if(view._id!==null){
      var e=document.getElementById(view._id);
    }
    switch(view.flag){
      case "add":
        this.__add(e,view);
        break;
      case "addDraggable":
        this.__addDraggable(e,view);
        break;
      case "set":
        this.__set(e,view);
        break;
      case "clear":
        this.__clear(e,view);
        break;
      case "showWidth":
        this.__showWidth(e,view);
        break;
      case "hideWidth":
        this.__hideWidth(e,view);
        break;
      case "setOnclick":
        this.__setOnclick(e,view);
        break;
      case "setOnclickNvalue":
        this.__setOnclickNvalue(e,view);
        break;
      case "destroy":
        this.__destroy(e,view);
        break;
      case "console":
        print(view.view);
        break;
      case "updateIfExists":
        print("heeeeeeeeeeeeeeeeeeeeeeeeeeej")
        print(view)
        this.__updateIfExists(e,view);
        break;
      case "cleanText":
        this.__cleanText(e,view);
        break;
    }
  }else{
    this.updateViews(view);
  }
}//}}}
def.__cleanText=function(element,view){//{{{
  element.value="";
}//}}}
def.__updateIfExists=function(element,view){//{{{
  if(element){
    this.__destroy(element,view);
  }
  var elem=document.getElementById(view.masterID);
  this.__add(elem,view);
}//}}}
def.__add=function(element,view){//{{{
  element.innerHTML+=view.view;
  element.scrollTop=element.scrollHeight;
}//}}}
def.__addDraggable=function(element,view){//{{{
  element.innerHTML+=view.view;
  $(".drag").draggable({
    containment: "body",
    scroll:false
  });
}//}}}
def.__destroy=function(element,view){//{{{
  element.parentElement.removeChild(element)
}//}}}
def.__set=function(element,view){//{{{
  element.innerHTML=view.view;
}//}}}
def.__clear=function(element,view){//{{{
  element.innerHTML="";
}//}}}
def.__showWidth=function(element,view){//{{{
  element.style.width=view.width;
}//}}}
def.__hideWidth=function(element,view){//{{{
  element.style.width=0;
}//}}}
def.__setOnclick=function(element,view){//{{{
  element.onclick=function(event){
    this.mb.send(event,view.command,view.args,view.respond);
  }.bind(this)
}//}}}
def.__setOnclickNvalue=function(element,view){//{{{
  element.innerHTML=view.value;
  element.onclick=function(event){
    this.mb.send(event,view.command,view.args,view.respond);
  }.bind(this)
}//}}}
def.updateViews=function(views){//{{{
  if(views.length!==undefined){
    views.forEach(function(view){
      this.updateView(view);
    }.bind(this))
  }else{
    this.updateView(views);
  }
  return this;
}//}}}
def.update=function(res){//{{{
  if(res.command.mode.respond==="view"){
    this.updateViews(res.result.view);
  }
}//}}}
//}}}
function mbForm(mb,formID,acceptID){//{{{
  this.mb=mb;
  this.form=document.getElementById(formID);
  this.acceptB=document.getElementById(acceptID);
  this.__prepare();
}
var def=mbForm.prototype;
def.htmlList2array=function toArray(obj){//{{{
  var array = [];
  for (var i = obj.length >>> 0; i--;){
    array[i] = obj[i];
  }
  return array;
}//}}}
def.keysNvalues2sl=function(keys,values){//{{{
  var SL={};
  keys.forEach(function(key,i){
    SL[key]=values[i];
  })
  return SL;
}//}}}
def.readObjectFromForm=function(keys){//{{{
  var result=[],
      inputs=this.htmlList2array(this.form.getElementsByTagName("input"));
  inputs.forEach(function(elem){
    result.push(elem.value)
  }.bind(this))
  return this.keysNvalues2sl(keys,result);
}//}}}
def.cleanInputs=function(bool){//{{{
  if(bool){
    var inputs=this.htmlList2array(this.form.getElementsByTagName("input"));
    inputs.forEach(function(elem){
      elem.value="";
    }.bind(this))
  }
}//}}}
//}}}
function mbRegistrationForm(mb){//{{{
  mbForm.call(this,mb,"mbRegisterForm","register")
}
var def=construct(mbForm,mbRegistrationForm);
def.__prepare=function(){//{{{
  this.acceptB.addEventListener("click",function(){
    var obj=this.readObjectFromForm([
      "name","surname","mail","mobile","password","confirmPassword"])
    this.mb.send('user.basic.registerUser',obj,{
        respond:"object",
        push:"object",
        type:"developer"
      },
      function(res){
        print(res)
        this.cleanInputs(res.data.bool);
        document.getElementById("mbFormComment").innerHTML=res.data.comment;
      }.bind(this))
  }.bind(this))
}//}}}
//}}}
function mbLoggingForm(mb){//{{{
  mbForm.call(this,mb,"mbLogInForm","logIn")
}
var def=construct(mbForm,mbLoggingForm);
def.__prepare=function(){//{{{
  this.acceptB.addEventListener("click",function(){
    var obj=this.readObjectFromForm(["mail","password"]);
    this.mb.send('user.basic.logIn',obj,{
          respond:"view",
          push:"object",
          type:"developer"
        },function(res){
          this.cleanInputs(res.data.bool);
          this.mb.viewC.updateView(res.data.view)
          document.getElementById("mbFormComment").innerHTML=res.data.comment;
      }.bind(this))
  }.bind(this))
}//}}}
//}}}
function mbLogOutForm(mb){//{{{
  mbForm.call(this,mb,"","logOut")
}
var def=construct(mbForm,mbLogOutForm);
def.__prepare=function(){//{{{
  this.acceptB.addEventListener("click",function(){
    this.mb.send('user.basic.logOut',{},{
        respond:"view",
        push:"object",
        type:"developer"
      },function(res){
        this.mb.viewC.updateView(res.data.view)
        document.getElementById("mbFormComment").innerHTML=res.data.comment;
      }.bind(this))
  }.bind(this))
}//}}}
//}}}
function mbMain(){//{{{
  document.mb=this;
  this.commandC=new mbCommandCenter(this,socket);
  this.viewC=new mbViewCenter(this);

  this.registerForm=new mbRegistrationForm(this);
  this.loggingForm=new mbLoggingForm(this);
  this.logOutForm=new mbLogOutForm(this);
}
var def=mbMain.prototype;
def.send=function(name,args,mode,cb){//{{{
  this.commandC.sendData2({
    name:name,
    args:args,
    mode:mode
  },cb)
  print(cb)
}//}}}
def.sendForView=function(event,name,args,mode){//{{{
  this.commandC.sendData2({
    name:name,
    args:args,
    mode:mode
  },function(res){
    this.viewC.update(res);
  }.bind(this))
}//}}}
//}}}


window.onload=function(){//{{{
  var mb=new mbMain();
}//}}}
