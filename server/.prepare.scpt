tell application "Terminal"
  activate
  tell application "System Events" to keystroke "t" using command down
  do script "vim -S .mySession.vim" in window 1
  delay 10
  tell application "System Events" to keystroke "t" using command down
  do script "sudo mongod --port 4501 --dbpath db" in window 1
  delay 10
  tell application "System Events" to keystroke "t" using command down
  do script "mongo --port 4501" in window 1
  delay 10
  tell application "System Events" to keystroke "t" using command down
  do script "sudo npm run server" in window 1
end tell
