//server/app.js
//mb FRAMEWORK{{{
'use strict'
//imports{{{
var express=require('express'),
    bodyParser=require("body-parser"),
    app=express(),
    server=require('http').Server(app),
    url=require('url'),
    terminal=require('child_process').execFile,
    fs=require('fs'),
    db=require('mongoose'),
    encoding=require("encoding"),
    PDF=require("pdfkit"),
    nodeMailer=require("nodemailer"),
    print=require("./utils/print"),
    display=require("./utils/print"),
    construct=require("./utils/construct"),
    serialize=require("serialize-javascript");
db.Promise=Promise;
var io=require('socket.io').listen(server);
var cleanTerminal=function(){//{{{
  let i=0;
  for(i=0;i<60;i++){
    display("");
  }
}//}}}
//}}}
//middleware{{{
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('client'));
//}}}
var settings=JSON.parse(fs.readFileSync("settings.json",'utf8')),
    SETTINGS=settings.MODE[settings.mode],
    DATA=settings.DATA;
//======================
//MODELS{{{
function mbManager(mb,name){//{{{
  this.mb=mb;
  this.name=name;
  this.objID=this.mb.db.Schema.ObjectId;
  this.__model=null;
}
var def=mbManager.prototype;
def.createModel=function(){//{{{
  return new Promise((resolve,reject)=>{
    resolve();
  })
}//}}}
def._createModel=function(obj){//{{{
  this.__model=this.mb.db.model(this.name,obj)
}//}}}
def.newModel=function(obj){//{{{
  return new this.__model(obj).save()
}//}}}
def.getModelBy=function(condition){//{{{
  return this.__model.findOne(condition).exec();
}//}}}
def.getModelBy_query=function(condition){//{{{
  return this.__model.findOne(condition);
}//}}}
def.delModelBy=function(condition){//{{{
  return this.__model.findOneAndRemove(condition).exec();
}//}}}
def.getModelsBy_query=function(condition){//{{{
  return this.__model.find(condition);
}//}}}
def.delModelsBy=function(condition){//{{{
  return this.__model.remove(condition).exec().then(doc=>{
    return doc;
  })
}//}}}
def.getModelsBy=function(condition){//{{{
  return this.getModelsBy_query(condition).exec();
}//}}}
def.delAllModels=function(){//{{{
  return this.__model.remove({}).exec().then(doc=>{
    return doc;
  })
}//}}}
def.getModelByID=function(_id){//{{{
  return this.getModelBy({
    _id:_id
  })
}//}}}
def.getAllModels=function(){//{{{
  return this.getModelsBy({});
}//}}}
def.showModelBy=function(condition){//{{{
  return this.getModelBy(condition)
    .then(doc=>{
      console.log(JSON.stringify(doc,undefined,2));
      return doc;
    })
}//}}}
def.showAllModels=function(){//{{{
  return this.getAllModels()
    .then(doc=>{
      console.log(JSON.stringify(doc,undefined,2));
      return doc;
    })
}//}}}
def.addItemToList=function(condition,item){//{{{
  this.__model.update(condition,{$push: item});
}//}}}
def.delModelByID=function(_id){//{{{
  return this.__model.remove({
    _id:_id
  }).exec().then(doc=>{
    return doc;
  })
}//}}}
//}}}
function mbValidator(mb,data){//{{{
  this.mb=mb;
  this.name=data.name;
  this.data=data;
}
var def=mbValidator.prototype;
//}}}
function mbCommand(mb,name){//{{{
  this.mb=mb;
  this.name=name;
}
var def=mbCommand.prototype;
def.__validate=function(UO,cmdData){//{{{
  let resultObject,
      p=Promise.resolve();
  cmdData.validators.map(validator=>{
    p=p.then(()=>{
      return this.mb.validator[validator].validate(UO);
    })
  });
  return p.then(doc=>{
    return this.run(UO)
      .then(doc=>{
        return new Promise((resolve,reject)=>{
          let respond=UO.command.mode.respond;
          if(respond==="view"){
            resolve(doc);
          }else if(respond==="object"){
            reject(doc);
          }
        }).then(doc=>{
          return this._genView(UO,doc)
            .then(doc=>{
              resultObject=this.__genRO(UO,{
                bool:true,
                comment:UO.command.data.comment.success,
                object:{},
                view:doc
              })
              return resultObject;
            })
        }).catch(err=>{
          resultObject=this.__genRO(UO,{
            bool:true,
            comment:UO.command.data.comment.success,
            object:err,//.object
            view:[]
          })
          return resultObject;
        })
      }).then(doc=>{
        return new Promise((resolve,reject)=>{
          let push=UO.command.mode.push;
          if(push==="view"){
            resolve(doc);
          }else if(push==="object"){
            reject(doc);
          }
        }).then(doc=>{
          return this._pushView(UO,doc);
        }).catch(err=>{
          return this._pushObject(UO,err);
        })
      })
  }).then(doc=>{
    return resultObject;
  }).catch(err=>{
    return this.__genRO(UO,{
      bool:err.bool,
      comment:err.comment,
      object:err.result,
      view:[]
    })
  })
}//}}}
def.__hasRole=function(role,roles){//{{{
  return (roles.indexOf(role)>=0);
}//}}}
def.__determineRole=function(UO,user,cmdData){//{{{
  return new Promise((resolve,reject)=>{
    if(user===null){
      reject();
    }else{
      resolve();
    }
  }).then(doc=>{
    UO.user._id=user._id;
    return this.mb.manager[cmdData.manager].getModelBy({
      userID:user._id
    });
  }).then(doc=>{
    return doc.role;
  }).catch(err=>{
    return "visitor";
  })
}//}}}
def.__runIfRoleIsCorrect=function(UO,data,user,cmdData){//{{{
  let role;
  return this.__determineRole(UO,user,cmdData)
    .then(doc=>{
      role=doc;
      UO.user.role=role;
      return new Promise((resolve,reject)=>{
        let result=this.__hasRole(role,cmdData.roles);
        if(result){
          resolve(result);
        }else{
          reject(result);
        }
      }).then(doc=>{
        return this.__validate(UO,cmdData)
      }).catch(err=>{
        return this.__genRO(UO,{
          bool:false,
          comment:`You have no permission to run '${data.name}' command.`,
          object:{},
          view:[]
        })
      })
    })
}//}}}
def.__mbROStyle=function(UO,result){//{{{
  return {
    user:{
      socketID:UO.user.socketID,
      role:UO.user.role
    },
    command:{
      name:UO.command.name,
      args:UO.command.args,
      mode:UO.command.mode
    },
    result:{
      bool:result.bool,
      comment:result.comment,
      view:result.view,
      object:result.object
    }
  }
}//}}}
def.__ooROStyle=function(UO,result){//{{{
  return {
    user:{
      socketID:UO.user.socketID,
      role:UO.user.role
    },
    command:{
      name:UO.command.name,
      args:UO.command.args,
      mode:UO.command.mode
    },
    data:{
      bool:result.bool,
      comment:result.comment,
      result:result.object,
      view:result.view
    }
  }
}//}}}
def.__genRO=function(UO,result){//{{{
  switch(SETTINGS.command.RO){
    case "mbRO":
      return this.__mbROStyle(UO,result);
    case "ooRO":
      return this.__ooROStyle(UO,result);
  }
}//}}}
def._run=function(socket,data,cb){//{{{
  let cmdData,user,brickUser,
      UO={
        user:{
          socket:socket,
          socketID:socket.id,
        },
        command:{
          name:data.name,
          args:data.args,
          /*mode:{
            respond:"object",
            push:"object",
            type:"developer"
          }*/
          mode:data.mode
        }
      };
  return this.mb.manager.Command.getModelByName(data.name)
    .then(doc=>{
      cmdData=doc;
      UO.command.data=doc;
      return this.mb.manager.User.getModelBySocketID(socket.id);
    }).then(doc=>{
      user=doc;
      return this.__runIfRoleIsCorrect(UO,data,user,cmdData)
    }).then(doc=>{
      let time=this.mb.system.Time.getCurrentTime();
      this.mb.system.Terminal.cyan(`[${time}] ==> Client receive object: ${JSON.stringify(doc)}`)
      this.mb.system.Terminal.green(JSON.stringify(doc.data.comment))
      return cb(doc);
    })
}//}}}

def._genView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def._pushView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def._pushObject=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def.run=function(UO){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
//}}}
function mbSystem(mb,name){//{{{
  this.mb=mb;
  this.name=name;
}
var def=mbSystem.prototype;
//}}}
function mbCenter(mb,name){//{{{
  this.mb=mb;
  this.name=name;
  this.__list=[];
  this.__SL={};
}
var def=mbCenter.prototype;
def.addItem=function(itemClass){//{{{
  let item=new itemClass(this.mb);
  this.__list.push(item);
  this.__SL[item.name]=item;
  return item;
}//}}}
def.getItemByName=function(name){//{{{
  return this.__SL[name];
}//}}}
def.addItems=function(items){//{{{
  items.forEach(itemClass=>{
    this.addItem(itemClass);
  })
  return this;
}//}}}
//}}}
function mbCase(mb,data){//{{{
  this.mb=mb;
  this.name=data.name;
  this.__commandName=data.commandName;
  this.__socket=data.socket;
  this.__args=data.args;
}
def=mbCase.prototype;
def.run=function(cb){//{{{
  switch(SETTINGS.scenario.command.mode){
    case "view":
      //socket,data,cb
      return this.mb.commandC.runCommand(this.__socket,{
        name:this.__commandName,
        args:this.__args,
        mode:{
          respond:"view",
          push:"view",
          type:"developer"
        }
      },cb);
    case "object":
      return this.mb.commandC.runCommand(this.__socket,{
        name:this.__commandName,
        args:this.__args,
        mode:{
          respond:"object",
          push:"object",
          type:"developer"
        }
      },cb);
  }
}//}}}
//}}}
function mbScenario(mb,data){//{{{
  this.mb=mb;
  this.name=data.name;
  this.cases=data.cases;
}
var def=mbScenario.prototype;
def.run=function(cb){//{{{
  let p=Promise.resolve();
  this.cases.map((name,i)=>{
    p=p.then(()=>{
      console.log(`${i+1}.`)
      return this.mb.caseC.runCase(name,cb);
    })
  })
  return p;
}//}}}
//}}}
function mbView(mb,name){//{{{
  this.mb=mb;
  this.name=name;
}
var def=mbView.prototype;
//}}}
//MODELS}}}
//SYSTEMS{{{
function mbTerminalSystem(mb){//{{{
  mbSystem.call(this,mb,"Terminal");
  this.__defaultBG="\x1b[40m";
  this.__defaultFG="\x1b[32m";
  this.__fgs={//{{{
    "black":"\x1b[30m",
    "red":"\x1b[31m",
    "green":"\x1b[32m",
    "yellow":"\x1b[33m",
    "blue":"\x1b[34m",
    "magenta":"\x1b[35m",
    "cyan":"\x1b[36m",
    "white":"\x1b[37m"
  };//}}}
  this.__bgs={//{{{
    "black":"\x1b[40m",
    "red":"\x1b[41m",
    "green":"\x1b[42m",
    "yellow":"\x1b[43m",
    "blue":"\x1b[44m",
    "magenta":"\x1b[45m",
    "cyan":"\x1b[46m",
    "white":"\x1b[47m"
  };//}}}
}
var def=construct(mbSystem,mbTerminalSystem);
def.__genColor=function(SL,color,defa){//{{{
  let result="";
  if(color===undefined){
    result+=defa;
  }else{
    result+=SL[color];
  }
  return result;
}//}}}
def.__genColors=function(fg,bg){//{{{
  let result=this.__genColor(this.__fgs,fg,this.__defaultFG);
  result+=this.__genColor(this.__bgs,bg,this.__defaultBG);
  return result;
}//}}}
def.show=function(text,fg,bg){//{{{
  process.stdout.write(this.__genColors(fg,bg)+text+this.__defaultFG+this.__defaultBG+"\n");
}//}}}
def.showStartServer=function(moduleName,host,port,dbPath,dbName){//{{{
  this.cleanScreen();
  console.log("run server");
  console.log("------------------------------------");
  console.log(`module  :  ${moduleName}`);
  console.log(`server  :  ${host}:${port+1}`);
  console.log(`    db  :  ${dbPath}/${dbName}:${port}`);
  console.log("------------------------------------");
}//}}}
def.cleanScreen=function(){//{{{
  for(let i=0;i<50;i++){
    console.log("\n");
  }
}//}}}
def.right=function(text){//{{{
  this.show(`✔  ${text}`,"green");
}//}}}
def.wrong=function(text){//{{{
  this.show(`✘  ${text}`,"red");
}//}}}
def.showTodoPoint=function(text){//{{{
  this.show("◦ "+text,"cyan");
}//}}}
def.showTodoList=function(todo){//{{{
  this.show("TODO","cyan");
  todo.forEach(point=>{
    this.showTodoPoint(point);
  })
}//}}}
def.secretInfo=function(text){//{{{
  this.show(`(${text})`,"yellow");
}//}}}
def.info=function(text){//{{{
  this.show(`--> ${text}`,"yellow");
}//}}}
def.magenta=function(text){//{{{
  this.show(`${text}`,"magenta");
}//}}}
def.cyan=function(text){//{{{
  this.show(`${text}`,"cyan");
}//}}}
def.white=function(text){//{{{
  this.show(`${text}`,"white");
}//}}}
def.green=function(text){//{{{
  this.show(`${text}`,"green");
}//}}}
def.pureText=function(color,text){//{{{
  //console.log(this.__genColors(fg,bg));
  process.stdout.write(this.__genColors(color)+text+this.__defaultFG+this.__defaultBG);
  return this;
}//}}}
def.endLine=function(){//{{{
  process.stdout.write("\n");
  return this;
}//}}}
//}}}
function mbJSONSystem(mb){//{{{
  mbSystem.call(this,mb,"JSON");
}
var def=construct(mbSystem,mbJSONSystem);
def.readJSONfile=function(JSONfile){//{{{
  return JSON.parse(fs.readFileSync(JSONfile,'utf8'));
}//}}}
//}}}
function mbCodeSystem(mb){//{{{
  mbSystem.call(this,mb,"Code");
  this.__signs="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#$%&".split("");
}
var def=construct(mbSystem,mbCodeSystem);
def.__genOneSign=function(){//{{{
  return this.__signs[Math.floor(Math.random()*this.__signs.length)];
}//}}}
def.genCode=function(amount){//{{{
  let code="";
  for(let i=0;i<amount;i++){
    code+=`${this.__genOneSign()}`
  }
  return code;
}//}}}
//}}}
function mbTimeSystem(mb){//{{{
  mbSystem.call(this,mb,"Time");
  this.days=["Niedziela","Poniedziałek","Wtorek","Środa","Czwartek","Piątek","Sobota"]
}
var def=construct(mbSystem,mbTimeSystem);
def.getCurrentTime=function(){//{{{
  let date=new Date(),
      text="";
  text+=date.getDate();
  text+="."+(date.getMonth()+1);
  text+="."+date.getFullYear();
  text+=" "+date.getHours();
  text+=":"+date.getMinutes();
  text+=":"+date.getSeconds();
  return text;
}//}}}
def.date2dayMonth=function(date){//{{{
  return (date.getDay()+1)+"."+(date.getMonth()+1);
}//}}}
def.getPolishDay=function(index){//{{{
  return this.days[index];
}//}}}
//}}}
//SYSTEMS}}}
//CENTERS{{{
function mbManagerCenter(mb){//{{{
  mbCenter.call(this,mb,"Manager");
}
var def=construct(mbCenter,mbManagerCenter);
def.getManager=function(name){//{{{
  return this.getItemByName(name);
}//}}}
def.createModels=function(){//{{{
  this.__list.forEach(manager=>{
    manager.createModel();
  })
  return this;
}//}}}
def.__delManager=function(manager){//{{{
  return manager.delAllModels();
}//}}}
def.cleanManagers=function(){//{{{
  return new Promise((resolve,reject)=>{
    let promises=this.__list.map(this.__delManager.bind(this));
    resolve(Promise.all(promises));
  })
}//}}}
def.addItem=function(data){//{{{
  return mbCenter.prototype.addItem.call(this,eval(`mb${data}Manager`));
}//}}}
//}}}
function mbValidatorCenter(mb){//{{{
  mbCenter.call(this,mb,"Validator");
}
var def=construct(mbCenter,mbValidatorCenter);
def.addItem=function(data){//{{{
  return mbCenter.prototype.addItem.call(this,eval(`mb${data.charAt(0).toUpperCase()}${data.slice(1)}Validator`));
}//}}}
//}}}
function mbCommandCenter(mb){//{{{
  mbCenter.call(this,mb,"Command");
}
var def=construct(mbCenter,mbCommandCenter);
def.__string2obj=function(data){//{{{
  if(typeof(data)=="string"){
    return JSON.parse(data);
  }else{
    return data;
  }
}//}}}
def.runCommand=function(socket,data,cb){//{{{
  let d=this.__string2obj(data),
      command=this.getItemByName(d.name),
      time=this.mb.system.Time.getCurrentTime();
  this.mb.system.Terminal.white(`[${time}] ==> Client ${socket.id} sends object: ${JSON.stringify(data)}`);
  if(command!==undefined){
    return command._run(socket,d,cb);
  }else{
    cb({
      response:{
        mode:"developer",
        type:"object"//view
      },
      command:{
        name:d.name,
        args:d.args
      },
      user:{
      },
      data:{
        bool:false,
        comment:`No such command as ${d.name}`,
        result:[],
        view:[]
      }
    })
  }
}//}}}
def.addItem=function(data){//{{{
  let command=mbCenter.prototype.addItem.call(this,eval(data.className));
  if(SETTINGS.command.show){
    console.log(data.name);
  }
  return this.mb.manager.Command.newModel(data);
}//}}}
def.__addItems=function(datas){//{{{
  let p=Promise.resolve();
  datas.map(data=>{
    p=p.then(()=>{
      return this.addItem(data);
    })
  })
  return p;
}//}}}
def.addItems=function(){//{{{
  let p=Promise.resolve();
  DATA.modules.map(data=>{
    p=p.then(()=>{
      return this.__addItems(DATA.COMMANDS[data]);
    })
  })
  return p;
}//}}}
//}}}
function mbSystemCenter(mb){//{{{
  mbCenter.call(this,mb,"System");
}
var def=construct(mbCenter,mbSystemCenter);
def.addItem=function(data){//{{{
  return mbCenter.prototype.addItem.call(this,eval(`mb${data}System`));
}//}}}
//}}}
function mbCommunicationCenter(mb){//{{{
  mbCenter.call(this,mb,"Communication");
}
var def=construct(mbCenter,mbCommunicationCenter);
def.prepareGETSET=function(){//{{{
  this.mb.app.get('/',(req,res)=>{
    res.sendfile(__dirname+"/"+SETTINGS.server.serv.index);
  })
  this.mb.app.get('/pdf',(req,res)=>{
    res.sendfile(__dirname+"/pdfs/pdfWithCode.pdf");
  })
  SETTINGS.server.serv.files.forEach(file=>{
    this.mb.app.get('/'+file,(req,res)=>{
      res.sendfile(__dirname+"/client/"+file+".html");
    })
  })
  return this;
}//}}}
def.prepareSocketIO=function(){//{{{
  this.mb.io.sockets.on("connection",socket=>{
    let time=this.mb.system.Time.getCurrentTime();
    console.log(`[${time}] ==> Client ${socket.id} was connected.`)
    //this.__onConnect(socket);
    socket.on("disconnect",()=>{
      console.log(`[${time}] ==> Client ${socket.id} was disconnected.`);
      //this.__onDisconnect(socket);
    })
    socket.on("sendData",(data,cb)=>{
      console.log("\x07");
      //display(this.getSystem("Time").getCurrentTime());
      print(cb)
      this.mb.commandC.runCommand(socket,data,cb);
    })
  })
  return this;
}//}}}
def.prepareProcess=function(){//{{{
  this.mb.process.on('cleanUp',()=>{
    display("--> server is about to shut down");
    //this.kickOffAllSockets();
  })
  this.mb.process.on('exit',()=>{
    this.mb.process.emit('cleanUp');
  });
  this.mb.process.on('SIGINT',()=>{
    this.mb.process.exit(2);
  });
  this.mb.process.on('uncaughtException', function(e) {
    console.log('Uncaught Exception...');
    console.log(e.stack);
    this.mb.process.exit(99);
  });
  this.mb.process.on('SIGTERM',()=>{
    this.mb.process.exit(0);
  });
  //process.stdin.resume();
  return this;
}//}}}
def.pushObject2all=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    if(UO.user.socket.id.indexOf("_socketID")>0){
      reject();
    }else{
    resolve();
    }
  }).then(doc=>{
    return UO.user.socket.broadcast.emit("fromOtherUser",data);
  }).catch(err=>{
    return true;
  })
}//}}}
def.pushObject2userByID=function(UO,tag,userID,object){//{{{
  let time=this.mb.system.Time.getCurrentTime(),
      user;
  return this.mb.manager.User.getModelByID(userID)
    .then(doc=>{
      user=doc;
      return new Promise((resolve,reject)=>{
        if(doc.socketID!==null){
          if(doc.socketID && doc.socketID.indexOf("_socketID")===-1){
            resolve(doc);
          }else{
            reject(doc);
          }
        }else{
          reject(doc);
        }
      }).then(doc=>{
        this.mb.system.Terminal.magenta(`[${time}] Server ==> ${user.socketID} pushed: ${JSON.stringify(object)}`);
        return this.mb.io.sockets.connected[user.socketID].emit(tag,object);
      }).catch(err=>{
        print(err)
        this.mb.system.Terminal.magenta(`[${time}] Server ==> ${user.socketID} cannot push: ${JSON.stringify(object)}`);
        return true;
      })
    })
}//}}}
def.pushObject2userByMail=function(UO,tag,userMail,object){//{{{
  let time=this.mb.system.Time.getCurrentTime(),
      user;
  return this.mb.manager.User.getModelByMail(userMail)
    .then(doc=>{
      user=doc;
      return new Promise((resolve,reject)=>{
        if(user.socketID!==null){
          if(doc.socketID && doc.socketID.indexOf("_socketID")===-1){
            resolve(doc);
          }else{
            reject(doc);
          }
        }else{
          reject(doc)
        }
      }).then(doc=>{
        this.mb.system.Terminal.magenta(`[${time}] Server ==> ${user.socketID} pushed: ${JSON.stringify(object.data)}`);
        return this.mb.io.sockets.connected[user.socketID].emit(tag,object);
      }).catch(err=>{
        this.mb.system.Terminal.magenta(`[${time}] Server ==> ${user.socketID} cannot push: ${JSON.stringify(object)}`);
        return true;
      })
    })
}//}}}
def.pushObject2users=function(UO,tag,users,object){//{{{
  let time=this.mb.system.Time.getCurrentTime(),
      p=Promise.resolve();
  print(object)
  users.map(user=>{
    p=p.then(()=>{
      print(user)
      return new Promise((resolve,reject)=>{
        if(user.socketID!==null){
          if(user.socketID.indexOf("_socketID")===-1){
            resolve(user);
          }else{
            reject(user);
          }
        }else{
          reject();
        }
      }).then(doc=>{
        
        this.mb.system.Terminal.magenta(`[${time}] Server ==> ${user.socketID} pushed: ${JSON.stringify(object)}`);
        return this.mb.io.sockets.connected[user.socketID].emit(tag,object);
      }).catch(err=>{
        this.mb.system.Terminal.magenta(`[${time}] Server ==> ${user.socketID} cannot push: ${JSON.stringify(object)}`);
        return true;
      })
    })
  })
  return p;
}//}}}
def.pushObject2systemUsers=function(UO,tag,users,object){//{{{
  let time=this.mb.system.Time.getCurrentTime(),
      p=Promise.resolve();
  print(object)
  users.map(user=>{
    p=p.then(()=>{
      print(user)
      return new Promise((resolve,reject)=>{
        if(user.userID.socketID!==null){
          if(user.userID.socketID.indexOf("_socketID")===-1){
            resolve();
          }else{
            reject();
          }
        }else{
          reject();
        }
      }).then(doc=>{
        
        this.mb.system.Terminal.magenta(`[${time}] Server ==> ${user.userID.socketID} pushed: ${JSON.stringify(object)}`);
        return this.mb.io.sockets.connected[user.userID.socketID].emit(tag,object);
      }).catch(err=>{
        this.mb.system.Terminal.magenta(`[${time}] Server ==> ${user.userID.socketID} cannot push: ${JSON.stringify(object)}`);
        return true;
      })
    })
  })
  return p;
}//}}}
def.pushObject2allSockets=function(UO,tag,data){//{{{
  return new Promise((resolve,reject)=>{
    if(UO.user.socket.id.indexOf("_socketID")>0){
      reject();
    }else{
      resolve();
    }
  }).then(doc=>{
    return this.mb.io.emit(tag,data);
  }).catch(err=>{
    return true;
  })
}//}}}
//}}}
function mbCaseCenter(mb){//{{{
  mbCenter.call(this,mb,"Case");
}
var def=construct(mbCenter,mbCaseCenter);
def.runCase=function(name,cb){//{{{
  return this.getItemByName(name).run(cb);
}//}}}
def.addItem=function(data){//{{{
  let cas=new mbCase(this.mb,data);
  this.__list.push(cas);
  this.__SL[cas.name]=cas;
  return cas;
}//}}}
def.__addItems=function(datas){//{{{
  datas.forEach(it=>{
    this.addItem(it);
  })
  return this;
}//}}}
def.addItems=function(){//{{{
  DATA.modules.forEach(it=>{
    this.__addItems(DATA.CASES[it]);
  })
  return this;
}//}}}
//}}}
function mbScenarioCenter(mb){//{{{
  mbCenter.call(this,mb,"Scenario");
  print(SETTINGS.scenario.output)
  switch(SETTINGS.scenario.output){
    case "object":
      this.__cb=function(res){
        console.log(res);
      }
      break;
    case "comment":
      this.__cb=function(res){
        if(SETTINGS.command.RO==="mbRO"){
          console.log(res.result.comment);
        }else if(SETTINGS.command.RO==="ooRO"){
          console.log(res.data.comment);
        }
      }
      break;
    case "JSON":
      this.__cb=function(res){
        console.log(JSON.stringify(res,undefined,2))
      }
      break;
    case "result":
      this.__cb=function(res){
        if(SETTINGS.command.RO==="mbRO"){
          console.log(res.result);
        }else if(SETTINGS.command.RO==="ooRO"){
          console.log(res.data.result);
        }
      }
      break;
    case "none":
      this.__cb=function(res){}
      break;
  }
}
var def=construct(mbCenter,mbScenarioCenter);
def.addItem=function(scenario){//{{{
  return this.mb.manager.Scenario.newModel(scenario);
}//}}}
def.addItems=function(scenarios){//{{{
  let p=Promise.resolve();
  scenarios.map(scenario=>{
    p=p.then(()=>{
      return this.addItem(scenario);
    })
  })
  return p;
}//}}}
def.run=function(){//{{{
  let p=Promise.resolve(),
      op=SETTINGS.scenario.option;
  if(op==="none"){
    p=p.then(()=>{
      return new Promise((resolve,reject)=>{
        resolve();
      });
    })
  }else if(op==="one"){
    p=p.then(()=>{
      return this.runScenario(SETTINGS.scenario.one);
    })
  }else if(op==="all"){
    p=p.then(()=>{
      return this.runAllScenarios();
    })
  }else if(op==="list"){
    p=p.then(()=>{
      return this.runSpecifiedScenarios(SETTINGS.scenario.list);
    })
  }
  return p;
}//}}}
def.runScenario=function(name){//{{{
  return this.mb.manager.Scenario.getModelByName(name)
    .then(doc=>{
      console.log(`    "START SCENARIO ${name}"`);
      let scenario=new mbScenario(this.mb,doc)
      return scenario.run(this.__cb);
    }).then(doc=>{
      console.log(`    "END SCENARIO ${name}"`);
      return doc;
    })
}//}}}
def.runAllScenarios=function(){//{{{
  return this.mb.manager.Scenario.getAllModels()
    .then(docs=>{
      let p=Promise.resolve(),
          scenario;
      docs.map(sc=>{
        p=p.then(()=>{
          scenario=new mbScenario(this.mb,sc)
          console.log(`    "START SCENARIO ${sc.name}"`);
          return scenario.run(this.__cb)
            .then(doc=>{
              console.log(`    "STOP SCENARIO ${sc.name}"`);
              return doc;
            })
        })
      })
      return p;
    })
}//}}}
def.runSpecifiedScenarios=function(list){//{{{
  let p=Promise.resolve(),
      scenario,
      scenarioM=this.mb.manager.Scenario;
  list.map(sc=>{
    p=p.then(()=>{
      return scenarioM.getModelByName(sc)
        .then(doc=>{
          scenario=new mbScenario(this.mb,doc);
          console.log(`    "START SCENARIO ${scenario.name}"`);
          return scenario.run(this.__cb)
            .then(doc=>{
              console.log(`    "STOP SCENARIO ${scenario.name}"`);
              return doc;
            })
        })
    })
  })
  return p;
}//}}}
//}}}
function mbViewCenter(mb){//{{{
  mbCenter.call(this,mb,"View");
}
var def=construct(mbCenter,mbViewCenter);
def.addItem=function(data){//{{{
  return mbCenter.prototype.addItem.call(this,eval(`mb${data}View`));
}//}}}
//}}}
//CENTERS}}}
function mbApp(){//{{{
  this.db=null;
}
var def=mbApp.prototype;
def.__build=function(app,io,process,db){//{{{
  return new Promise((resolve,reject)=>{
    this.app=app;
    this.db=db;
    this.io=io;
    this.process=process;
    this.managerC=new mbManagerCenter(this);
    this.systemC=new mbSystemCenter(this);
    this.validatorC=new mbValidatorCenter(this);
    this.caseC=new mbCaseCenter(this);
    this.scenarioC=new mbScenarioCenter(this);
    this.viewC=new mbViewCenter(this);
    this.commandC=new mbCommandCenter(this);
    this.communicationC=new mbCommunicationCenter(this);
    this.communicationC
      .prepareGETSET()
      .prepareSocketIO()
    resolve();
  }).then(doc=>{
    return this.managerC.addItems(DATA.MANAGERS);
  }).then(doc=>{
    return this.managerC.createModels();
  }).then(doc=>{
    return this.managerC.cleanManagers();
  }).then(doc=>{
    return this.systemC.addItems(DATA.SYSTEMS);
  }).then(doc=>{
    return this.validatorC.addItems(DATA.VALIDATORS);
  }).then(doc=>{
    return this.viewC.addItems(DATA.VIEWS);
  }).then(doc=>{
    return this.commandC.addItems();
  }).then(doc=>{
    return this.caseC.addItems();
  }).then(doc=>{
    return this.scenarioC.addItems(DATA.SCENARIOS);
  }).then(doc=>{
    return this.scenarioC.run();
  }).then(doc=>{
    return this.managerC.cleanManagers()
  }).then(doc=>{
    return this.commandC.addItems();
  }).then(doc=>{
    return this.prepareDB();
  }).then(doc=>{
    return this.runDeveloperTest();
  })
}//}}}
//manager{{{
Object.defineProperty(def,"manager",{
  get:function(){
    return this.managerC.__SL;
  }
})//}}}
//system{{{
Object.defineProperty(def,"system",{
  get:function(){
    return this.systemC.__SL;
  }
})//}}}
//validator{{{
Object.defineProperty(def,"validator",{
  get:function(){
    return this.validatorC.__SL;
  }
})//}}}
//scenario{{{
Object.defineProperty(def,"scenario",{
  get:function(){
    return this.scenarioC.__SL;
  }
})//}}}
//case{{{
Object.defineProperty(def,"case",{
  get:function(){
    return this.caseC.__SL;
  }
})//}}}
//view{{{
Object.defineProperty(def,"view",{
  get:function(){
    return this.viewC.__SL;
  }
})//}}}
def.__genMongoDBaddress=function(){//{{{
  let db=SETTINGS.db;
  return `mongodb:/`+`/${db.address}:${db.port}/${db.name}`
}//}}}
def.__displayServerIsWorking=function(){//{{{
  let server=SETTINGS.server;
  display(`✔ Server is working: ${server.address}:${server.port}`);
}//}}}
def.__displayDBIsWorking=function(){//{{{
  let db=SETTINGS.db;
  display(`✔ DB ${db.name} is working: ${db.address}:${db.port}`);
}//}}}
def.run=function(devTest){//{{{
  this.__devTest=devTest;
  cleanTerminal();
  server.listen(SETTINGS.server.port,()=>{
    this.__displayServerIsWorking();
    db.connect(this.__genMongoDBaddress(),{useMongoClient:true},()=>{
      this.__displayDBIsWorking();
      this.__build(app,io,process,db);
    })
  });
}//}}}
def.runCommand=function(socket,name,args,mode){//{{{
  return this.commandC.runCommand(socket,{
      name:"system.basic.test",
      args:{
        name:'marco'
      },
      mode:{
        respond:'view',
        push:'view',
        type:'developer'
      }
    },function(res){
      print("result")
      print(res);
    })
}//}}}
def.prepareDB=function(){//{{{
  let p=Promise.resolve(),
      op=SETTINGS.db.prepare.option;
  if(op==="none"){
    p=p.then(()=>{
      return new Promise((resolve,reject)=>{
        resolve();
      });
    })
  }else if(op==="one"){
    p=p.then(()=>{
      return this.scenarioC.addItems(DATA.SCENARIOS)
        .then(doc=>{
          console.log("==> Prepared scenarios for work:");
          return this.scenarioC.runScenario(SETTINGS.db.prepare.one);
        })
    })
  }else if(op==="list"){
    p=p.then(()=>{
      return this.scenarioC.addItems(DATA.SCENARIOS)
        .then(doc=>{
          console.log("==> Prepared scenarios for work:");
          return this.scenarioC.runSpecifiedScenarios(SETTINGS.db.prepare.list);
        })
    })
  }
  return p;
}//}}}
def.runDeveloperTest=function(){//{{{
  return this.__devTest();
}//}}}
//}}}
//MANAGERS{{{
function mbCommandManager(mb){//{{{
  mbManager.call(this,mb,"Command");
}
var def=construct(mbManager,mbCommandManager);
def.createModel=function(){//{{{
  return this._createModel({
    className:{
      type:String
    },
    name:{
      type:String
    },
    manager:{
      type:String
    },
    roles:[{
      type:String
    }],
    comment:{
      fail:{
        type:String
      },
      success:{
        type:String
      }
    },
    validators:[{
      type:String
    }]
  })
}//}}}
def.getModelByName=function(name){//{{{
  return this.getModelBy({
    name:name
  })
}//}}}
//}}}
function mbUserManager(mb){//{{{
  mbManager.call(this,mb,"User");
}
var def=construct(mbManager,mbUserManager);
def.createModel=function(){//{{{
  return this._createModel({
    name:{
      type:String,
    },
    surname:{
      type:String,
    },
    mail:{
      type:String,
    },
    password:{
      type:String,
    },
    mobile:{
      type:String,
    },
    socketID:{
      type:String
    }
  })
}//}}}
def.getModelBySocketID=function(socketID){//{{{
  return this.getModelBy({
    socketID:socketID
  })
}//}}}
def.getModelByMail=function(mail){//{{{
  return this.getModelBy({
    mail:mail
  })
}//}}}
def.logIn=function(UO){//{{{
  let d=UO.command.args,
      user;
  return this.getModelBy({
    mail:d.mail,
    password:d.password
  }).then(doc=>{
    doc.socketID=UO.user.socketID;
    return doc.save();
  }).then(doc=>{
    user=doc;
    return this.mb.manager.SystemUser.getModelBy({
      userID:user._id
    })
  }).then(doc=>{
    return new Promise((resolve,reject)=>{
      resolve({
        name:user.name,
        surname:user.surname,
        password:user.password,
        mobile:user.mobile,
        mail:user.mail,
        role:doc.role
      })
    })
  }).catch(err=>{
    print(err)
  })
}//}}}
def.logOut=function(UO){//{{{
  let d=UO.command.args;
  return this.getModelByID(UO.user._id)
    .then(doc=>{
      doc.socketID=null;
      return doc.save();
    })
}//}}}
def.registerUser=function(UO){//{{{
  let d=UO.command.args,
      user;
  return this.newModel({
    name:d.name,
    surname:d.surname,
    mail:d.mail,
    mobile:d.mobile,
    password:d.password
  }).then(doc=>{
    let user=doc;
    return this.mb.manager.SystemUser.newModel({
      role:"user",
      userID:user
    })
  })
}//}}}

def.isMailUnique=function(UO){//{{{
  let d=UO.command.args;
  return this.getModelByMail(d.mail)
    .then(doc=>{
      return new Promise((resolve,reject)=>{
        if(doc!==null){
          resolve()
        }else{
          reject()
        }
      }).then(doc=>{
        return true;
      }).catch(err=>{
        return false;
      })
    })
}//}}}

//}}}
function mbSystemUserManager(mb){//{{{
  mbManager.call(this,mb,"SystemUser");
}
var def=construct(mbManager,mbSystemUserManager);
def.createModel=function(){//{{{
  return this._createModel({
    userID:{
      type:this.objID,
      ref:"User"
    },
    role:{
      type:String
    },
    estateID:{
      type:this.objID,
      ref:"Estate",
      required:false
    }
  })
}//}}}
//}}}
function mbScenarioManager(mb){//{{{
  mbManager.call(this,mb,"Scenario");
}
var def=construct(mbManager,mbScenarioManager);
def.createModel=function(){//{{{
  return this._createModel({
    name:{
      type:String
    },
    cases:[{
      type:String
    }]
  })
}//}}}
def.getModelByName=function(name){//{{{
  return this.getModelBy({
    name:name
  })
}//}}}
//}}}
//MANAGERS}}}
//PATTERNS{{{
function mbPatternManager(mb){//{{{
  mbManager.call(this,mb,"Pattern");
}
var def=construct(mbManager,mbPatternManager);
def.createModel=function(){//{{{
  return this._createModel({})
}//}}}
//}}}
function mbPatternCommand(mb){//{{{
  mbCommand.call(this.mb,"system.basic.pattern");
}
var def=construct(mbCommand,mbPatternCommand);
def._genView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def._pushView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def._pushObject=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def.run=function(UO){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
//}}}
//PATTERNS}}}
//mb FRAMEWORK}}}
//======================
//WORKSPACE{{{
//MANAGERS{{{

//MANAGERS}}}
//VALIDATORS{{{
function mbNameHasLengthOf5Validator(mb){//{{{
  mbValidator.call(this,mb,{
    name:"nameHasLengthOf5",
    comment:"Name has not length of 5."
  })
}
var def=construct(mbValidator,mbNameHasLengthOf5Validator);
def.validate=function(UO){//{{{
  return new Promise((resolve,reject)=>{
    if(UO.command.args.name.length===5){
      resolve({})
    }else{
      reject({
        comment:this.data.comment,
        bool:false,
        result:{}
      })
    }
  })
}//}}}
//}}}
function mbIsMailUniqueValidator(mb){//{{{
  mbValidator.call(this,mb,{
    name:"isMailUnique",
    comment:"Mail is not unique."
  })
}
var def=construct(mbValidator,mbIsMailUniqueValidator);
def.validate=function(UO){//{{{
  let d=UO.command.args;
  return new Promise((resolve,reject)=>{
    resolve()
  }).then(doc=>{
    return this.mb.manager.User.getModelBy({
      mail:d.mail
    });
  }).then(doc=>{
    return new Promise((resolve,reject)=>{
      if(doc===null){
        resolve({})
      }else{
        reject({
          comment:this.data.comment,
          bool:false,
          result:{}
        })
      }
    })
  })
}//}}}
//}}}

function mbMailExistsValidator(mb){//{{{
  mbValidator.call(this,mb,{
    name:"mailExists",
    comment:"Such mail is not existing."
  })
}
var def=construct(mbValidator,mbMailExistsValidator);
def.validate=function(UO){//{{{
  let d=UO.command.args;
  return this.mb.manager.User.getModelBy({
    mail:d.mail
  }).then(doc=>{
    return new Promise((resolve,reject)=>{
      if(doc!==null){
        resolve({})
      }else{
        reject({
          comment:this.data.comment,
          bool:false,
          result:{}
        })
      }
    })
  })
}//}}}
//}}}
function mbMailIsCorrectValidator(mb){//{{{
  mbValidator.call(this,mb,{
    name:"mailIsCorrect",
    comment:"Such mail is not correct."
  })
}
var def=construct(mbValidator,mbMailIsCorrectValidator);
def.validate=function(UO){//{{{
  let d=UO.command.args,
      re=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      result=re.test(d.mail);
  return new Promise((resolve,reject)=>{
    if(result){
      resolve({})
    }else{
      reject({
        comment:this.data.comment,
        bool:false,
        result:{}
      })
    }
  })
}//}}}
//}}}
function mbRegisterDataIsNotEmptyValidator(mb){//{{{
  mbValidator.call(this,mb,{
    name:"registerDataIsNotEmpty",
    comment:"You have not sent all data for registration."
  })
}
var def=construct(mbValidator,mbRegisterDataIsNotEmptyValidator);
def.validate=function(UO){//{{{
  let d=UO.command.args;
  return new Promise((resolve,reject)=>{
    if(d.name==="" || d.surname==="" || d.mail==="" || d.mobile==="" || d.password==="" || d.confirmPassword===""){
      reject({
        comment:this.data.comment,
        bool:false,
        result:{}
      })
    }else{
      resolve({})
    }
  })
}//}}}
//}}}
function mbMailIsUniqueValidator(mb){//{{{
  mbValidator.call(this,mb,{
    name:"mailIsUnique",
    comment:"Such mail is not unique."
  })
}
var def=construct(mbValidator,mbMailIsUniqueValidator);
def.validate=function(UO){//{{{
  let d=UO.command.args;
  return this.mb.manager.User.getModelBy({
    mail:d.mail
  }).then(doc=>{
    return new Promise((resolve,reject)=>{
      if(doc===null){
        resolve({})
      }else{
        reject({
          comment:this.data.comment,
          bool:false,
          result:{}
        })
      }
    })
  })
}//}}}
//}}}
function mbConfirmPasswordMatchesPasswordValidator(mb){//{{{
  mbValidator.call(this,mb,{
    name:"confirmPasswordMatchesPassword",
    comment:"Introduced passwords are not similar."
  })
}
var def=construct(mbValidator,mbConfirmPasswordMatchesPasswordValidator);
def.validate=function(UO){//{{{
  let d=UO.command.args;
  return new Promise((resolve,reject)=>{
    if(d.password===d.confirmPassword){
      resolve({})
    }else{
      reject({
        comment:this.data.comment,
        bool:false,
        result:{}
      })
    }
  })
}//}}}
//}}}
function mbPasswordMatchesMailValidator(mb){//{{{
  mbValidator.call(this,mb,{
    name:"passwordMatchesMail",
    comment:"You have introduced wrong password."
  })
}
var def=construct(mbValidator,mbPasswordMatchesMailValidator);
def.validate=function(UO){//{{{
  let d=UO.command.args;
  return this.mb.manager.User.getModelBy({
    mail:d.mail,
    password:d.password
  }).then(doc=>{
    return new Promise((resolve,reject)=>{
      if(doc!==null){
        resolve({})
      }else{
        reject({
          comment:this.data.comment,
          bool:false,
          result:{}
        })
      }
    })
  })
}//}}}
//}}}
function mbLoggingDataIsNotEmptyValidator(mb){//{{{
  mbValidator.call(this,mb,{
    name:"loggingDataIsNotEmpty",
    comment:"You have not sent all data for logging."
  })
}
var def=construct(mbValidator,mbLoggingDataIsNotEmptyValidator);
def.validate=function(UO){//{{{
  let d=UO.command.args;
  return new Promise((resolve,reject)=>{
    if(d.mail==="" || d.password===""){
      reject({
        comment:this.data.comment,
        bool:false,
        result:{}
      })
    }else{
      resolve({})
    }
  })
}//}}}
//}}}


function mbUserIsAlreadyLoggedValidator(mb){//{{{
  mbValidator.call(this,mb,{
    name:"userIsAlreadyLogged",
    comment:"You are already logged."
  })
}
var def=construct(mbValidator,mbUserIsAlreadyLoggedValidator);
def.validate=function(UO){//{{{
  let d=UO.command.args;
  return this.mb.manager.User.getModelByMail(d.mail)
    .then(doc=>{
      return new Promise((resolve,reject)=>{
        if(d.socketID===null){
          resolve({})
        }else{
          reject({
            comment:this.data.comment,
            bool:false,
            result:{}
          })
        }
      })
    })
  
}//}}}
//}}}

//VALIDATORS}}}
//COMMANDS{{{
//client validation{{{
function mbIsMailUniqueCommand(mb){//{{{
  mbCommand.call(this,mb,"system.validation.isMailUnique");
}
var def=construct(mbCommand,mbIsMailUniqueCommand);
def._genView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def._pushView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def._pushObject=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def.run=function(UO){//{{{
  return this.mb.manager.User.isMailUnique(UO)
}//}}}
//}}}
//client validation}}}
//basic{{{
function mbTestCommand(mb){//{{{
  mbCommand.call(this,mb,"system.basic.test");
}
var def=construct(mbCommand,mbTestCommand);
def._genView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve([{
      flag:"set",
      _id:"testSlot",
      view:`<p>your result:${JSON.stringify(data)}</p>`
    }])
  })
}//}}}
def._pushView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve([{
      flag:"set",
      _id:"testPushSlot",
      view:`<p>your received push</p>`
    }])
  }).then(doc=>{
    return this.mb.communicationC.pushObject2all(UO,{
      view:doc,
      object:{}
    });
  })
}//}}}
def._pushObject=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve({
      x:"hejo"
    })
  }).then(doc=>{
    return this.mb.communicationC.pushObject2all(UO,{
      view:[],
      object:doc
    });
  })
}//}}}
def.run=function(UO){//{{{
  return new Promise((resolve,reject)=>{
    resolve({
      bool:true,
      result:{x:"ok"},
      comment:UO.command.data.comment.success
    })
  })
}//}}}
//}}}
function mbLogInCommand(mb){//{{{
  mbCommand.call(this,mb,"user.basic.logIn");
}
var def=construct(mbCommand,mbLogInCommand);
def._genView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve([
      this.mb.view.UserData.draw(UO,data)
    ])
  })
}//}}}
def._pushView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def._pushObject=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def.run=function(UO){//{{{
  return this.mb.manager.User.logIn(UO);
}//}}}

//}}}
function mbLogOutCommand(mb){//{{{
  mbCommand.call(this,mb,"user.basic.logOut");
}
var def=construct(mbCommand,mbLogOutCommand);
def._genView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve([
      this.mb.view.UserData.clean(UO,data)
    ])
  })
}//}}}
def._pushView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def._pushObject=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def.run=function(UO){//{{{
  return this.mb.manager.User.logOut(UO)
}//}}}
//}}}
function mbRegisterUserCommand(mb){//{{{
  mbCommand.call(this,mb,"user.basic.registerUser");
}
var def=construct(mbCommand,mbRegisterUserCommand);
def._genView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def._pushView=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def._pushObject=function(UO,data){//{{{
  return new Promise((resolve,reject)=>{
    resolve()
  })
}//}}}
def.run=function(UO){//{{{
  return this.mb.manager.User.registerUser(UO)
}//}}}
//}}}

//basic}}}
//COMMANDS}}}
//VIEWS{{{
function mbUserDataView(mb){
  mbView.call(this,mb,"UserData");
}
var def=construct(mbView,mbUserDataView);
def.draw=function(UO,data){//{{{
  let view=`
    <div class='mbFormItem'><span>Name</span><span>:</span><span>${data.name}</span></div>
    <div class='mbFormItem'><span>Surname</span><span>:</span><span>${data.surname}</span></div>
    <div class='mbFormItem'><span>Mail</span><span>:</span><span>${data.mail}</span></div>
    <div class='mbFormItem'><span>Role</span><span>:</span><span>${data.role}</span></div>
    <div class='mbFormItem'><span>Mobile</span><span>:</span><span>${data.mobile}</span></div>
    <div class='mbFormItem'><span>Password</span><span>:</span><span>${data.password}</span></div>
    `;
  return {
    flag:"set",
    view:view,
    _id:"mbDataSlot"
  }
}//}}}
def.clean=function(UO,data){//{{{
  let view="";
  return {
    flag:"set",
    view:view,
    _id:"mbDataSlot"
  }
}//}}}
//VIEWS}}}
//WORKSPACE}}}

//======================
//RUN{{{
var mb=new mbApp();
mb.run(function(){
  return new Promise((resolve,reject)=>{
    resolve();
  })
}.bind(mb));
//RUN}}}
